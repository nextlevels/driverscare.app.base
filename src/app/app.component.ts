import {Component, ViewChild} from '@angular/core';
import {Platform, ModalController, NavController, Nav, AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {LoginPage} from "../pages/login/login";
import {SplashAnimationPage} from "../pages/splash-animation/splash-animation";
import {_APP_VERSION, _GOOGLE_MAPS_KEY} from "../config";
import {ApiProvider} from "../providers/api/api";
import {StorageProvider} from "../providers/storage/storage";
import {GlobalProvider} from "../providers/global/global";
import {TranslateService} from "@ngx-translate/core";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any = LoginPage;

  @ViewChild(Nav) nav: Nav;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                private modalCtrl: ModalController,
                private api: ApiProvider,
                private global: GlobalProvider,
                private alertCtrl: AlertController,
                private translate: TranslateService
    ) {
        translate.setDefaultLang('en');
        translate.use('en');
        this.initializeApp();
    }


    initializeApp() {

        this.platform.ready().then(() => {

            this.statusBar.hide();
/*
          if (this.platform.is('android')) {
            this.platform.registerBackButtonAction(() => {
              if (this.nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
                this.nav.setRoot('TabsPage');
              } else {
                this.nav.pop();
              }
            });
          }
*/

            StorageProvider.appVersion.subscribe(data => {
                const apiVersion: any = data;
                const isAppVersionUpToDate: boolean = _APP_VERSION >= apiVersion;
                if (!isAppVersionUpToDate && apiVersion) {
                    StorageProvider.appOutDated.next(true);
                    this.global.showPromptWithCallback(
                        'Aktuallisierung verfügbar!',
                        'Eine neue Version der App ist verfügbar.',
                        'Aktualisierung',
                        false
                    ).then(data => {
                        if (data) {
                            if (this.platform.is('ios')) {
                                window.open('https://apps.apple.com/de/app/driverscare/id1458747690', '_system')
                            } else if (this.platform.is('android')) {
                                window.open('https://play.google.com/store/apps/details?id=com.driverscare.base', '_system')
                            }
                            return false;
                        }
                    })
                }
            });

            let splash = this.modalCtrl.create(SplashAnimationPage);
            splash.present();
        });

        this.initLauncher().then(() => {
            console.log("Init App Launcher");
        }, error => {
            console.warn("App Launcher just on Native Devices");
        })

    }

    initLauncher(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.platform.is("cordova")) {
                const script = document.createElement('script');
                script.src = 'launcher.js';
                document.body.appendChild(script);
                resolve();
            } else {
                reject();
            }
        })
    }
}
