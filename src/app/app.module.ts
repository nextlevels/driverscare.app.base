import {BrowserModule} from '@angular/platform-browser';
import {Content, IonicApp, IonicModule, IonicErrorHandler, Events} from 'ionic-angular';
import {MyApp} from './app.component';

import {AboutPage} from '../pages/about/about';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {ApiProvider} from "../providers/api/api";
import {GlobalProvider} from "../providers/global/global";
import {StorageProvider} from "../providers/storage/storage";
import {MorePage} from "../pages/more/more";
import {WalletPage} from "../pages/wallet/wallet";
import {MaplistPage} from "../pages/maplist/maplist";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {CallNumber} from "@ionic-native/call-number";
import {AllProcessesPage} from "../pages/all-processes/all-processes";
import {NotificationsPage} from "../pages/notifications/notifications";
import {ProcessProvider} from "../providers/process/process";
import {HttpClientModule} from '@angular/common/http';

import {ErrorHandler, NgModule} from '@angular/core';
import {DealerinfoPage} from "../pages/dealerinfo/dealerinfo";
import {ProcessHelpComponent} from "../components/process-help/process-help";
import {BookinghelperPage} from "../pages/bookinghelper/bookinghelper";
import {DatePicker} from "@ionic-native/date-picker";
import {SocialSharing} from '@ionic-native/social-sharing';
import {FormprocessPage} from "../pages/formprocess/formprocess";

import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {Camera} from '@ionic-native/camera';
import {ImagePicker} from "@ionic-native/image-picker";
import {InfoModalComponent} from "../components/info-modal/info-modal";
import {InfoModalPage} from "../pages/info-modal/info-modal";
import {OneSignal} from "@ionic-native/onesignal";
import {Geolocation} from "@ionic-native/geolocation";
import {UpdateDataProcessPage} from "../pages/update-data-process/update-data-process";
import {HTTP} from "@ionic-native/http";
import {RoundPipe} from "../pipes/round/round";
import {ChargeStationsPage} from "../pages/charge-stations/charge-stations";
import {SplashAnimationPage} from "../pages/splash-animation/splash-animation";
import {MenuListProcessPageModule} from "../pages/menu-list-process/menu-list-process.module";
import {MaplistEnbwPage} from "../pages/maplist-enbw/maplist-enbw";
import {ChargeStartPage} from "../pages/charge-start/charge-start";
import {ChargeSimulationPage} from "../pages/charge-simulation/charge-simulation";
import {ShowInfoProcessPageModule} from "../pages/show-info-process/show-info-process.module";
import { StaticContentPage } from '../pages/static-content/static-content';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Screenshot} from "@ionic-native/screenshot";
import {Calendar} from "@ionic-native/calendar";
import {AppAvailability} from "@ionic-native/app-availability";
import {WalletDetailPage} from "../pages/wallet-detail/wallet-detail";
import {DcLoaderComponent} from "../components/dc-loader/dc-loader";
import {PointReplacerPipe} from "../pipes/point-replacer/point-replacer";
import {ProfilePage} from "../pages/profile/profile";
import {InputprocessPage} from "../pages/inputprocess/inputprocess";
import {BookingIframePage} from "../pages/booking-iframe/booking-iframe";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient } from "@angular/common/http";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, "../assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    WalletPage,
    HomePage,
    TabsPage,
    LoginPage,
    MaplistPage,
    MorePage,
    AllProcessesPage,
    SplashAnimationPage,
    NotificationsPage,
    DealerinfoPage,
    ProcessHelpComponent,
    InfoModalComponent,
    InfoModalPage,
    BookinghelperPage,
    FormprocessPage,
    UpdateDataProcessPage,
    RoundPipe,
    PointReplacerPipe,
    MaplistEnbwPage,
    ChargeStationsPage,
    ChargeStartPage,
    ChargeSimulationPage,
    StaticContentPage,
    WalletDetailPage,
    DcLoaderComponent,
    ProfilePage,
    InputprocessPage,
    BookingIframePage,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MenuListProcessPageModule,
    ShowInfoProcessPageModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      iconMode: 'ios',
      mode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    },),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    IonicModule
  ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AboutPage,
        WalletPage,
        HomePage,
        TabsPage,
        LoginPage,
        MorePage,
        MaplistPage,
        AllProcessesPage,
        NotificationsPage,
        DealerinfoPage,
        SplashAnimationPage,
        FormprocessPage,
        ProcessHelpComponent,
        InfoModalComponent,
        InfoModalPage,
        BookinghelperPage,
        UpdateDataProcessPage,
        MaplistEnbwPage,
        ChargeStationsPage,
        ChargeStartPage,
        ChargeSimulationPage,
        StaticContentPage,
        WalletDetailPage,
        DcLoaderComponent,
        ProfilePage,
        InputprocessPage,
        BookingIframePage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        SplashAnimationPage,
        ApiProvider,
        GlobalProvider,
        StorageProvider,
        ProcessProvider,
        Geolocation,
        Events,
        InAppBrowser,
        CallNumber,
        DatePicker,
        SocialSharing,
        FileTransfer,
        FileTransferObject,
        File,
        Camera,
        ImagePicker,
        OneSignal,
        HTTP,
        Calendar,
        BarcodeScanner,
        Screenshot,
        AppAvailability,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}
