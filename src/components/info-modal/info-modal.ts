import {Component, Input, SimpleChanges} from '@angular/core';
import {ModalController} from "ionic-angular";
import {InfoModalPage} from "../../pages/info-modal/info-modal";
import {StorageProvider} from "../../providers/storage/storage";

@Component({
    selector: 'info-modal',
    templateUrl: 'info-modal.html'
})
export class InfoModalComponent {

    @Input() test: boolean = false;
    showHelp: boolean = false;

    constructor(private modal: ModalController) {
      if(StorageProvider.processPolicy != ''){
        this.showHelp = true;
      }
    }

    private openModal(){
        this.modal.create(InfoModalPage, {}).present()
    }

}
