import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NavController, Tabs} from "ionic-angular";

@Component({
  selector: 'process-help',
  templateUrl: 'process-help.html'
})
export class ProcessHelpComponent {

  tab : Tabs;
  @Input() showToast : boolean = false;
  @Output() closeToast : EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
      private navCtrl : NavController
  ) {
      this.tab = this.navCtrl.parent;
  }

    goToTabMySeries(){
        this.f_closeToast();
        this.tab.select(2);
    }

    f_closeToast(){
        this.showToast = false;
        this.closeToast.emit(this.showToast);
    }

}
