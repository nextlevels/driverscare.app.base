import { Component } from '@angular/core';

@Component({
  selector: 'dc-loader',
  templateUrl: 'dc-loader.html'
})
export class DcLoaderComponent {

  text: string;

  constructor() {
    console.log('Hello DcLoaderComponent Component');
    this.text = 'Hello World';
  }

}
