/**
 *
 * @type {{test: string; live: string}}
 * @private
 */
const _API_URL = {
    test: "http://portal.drivers-care.com/api",
    live: "http://portal.drivers-care.com/api"
};
export const _GOOGLE_MAPS_KEY = "AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI";
export const _APP_VERSION = "2.0.1";

/**
 * Testmode for Developing
 * @type {boolean}
 * @private
 */
export const _TEST_MODE: boolean = false;

/**
 * TEST MODE Settings - default is always true! Only works when TEST MODE True!
 * @type {{user: boolean; all_processes: boolean; no_caching: boolean; auto_login: boolean}}
 * @private
 */
export const _TEST_MODE_SETTINGS = {
    //testuser with auto fill
    test_user_auto_fill : false,
    //all processes instead of assigned
    all_processes : false,
    //caching
    no_caching : false, //TODO
    //logged in with TEST_USER and skip login maskcard-title
    auto_login : false,
    //Take Test API Url
    api_test_url : true,
    //Dealer Email by request meeting
    dealer_test : false
};


/**
 *
 * @type {{email: string; password: string}}
 * @private
 */
export const _TEST_USER : any = {
    email : 'paul.kalisch@next-levels.de', //jan-dc@next-levels.de //florian.drive@next-levels.de //c.wollnik@wollnik-gandlau.systems
    password : '12345',
    dealer_email : 'dc@next-levels.de'
};

/**
 *
 * @type {string}
 * @private
 */
export const _BASE_URL: string = (_TEST_MODE && _TEST_MODE_SETTINGS.api_test_url) ? _API_URL.test : _API_URL.live;

