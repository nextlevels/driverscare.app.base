import {Pipe, PipeTransform} from '@angular/core';

/**
 * Generated class for the RoundPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
    name: 'round',
})
export class RoundPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     */
    transform(value: string, ...args) {
        if (args[0] === 'kw') {
            return Number(value).toFixed(1) + ' kW';
        }
        if (args[0] === 'km') {
            return (Math.round(Number(value)) / 1000).toFixed(1) + ' KM'
        }
    }
}
