import {Injectable} from '@angular/core';
import {AlertController, NavController, Platform} from "ionic-angular";


@Injectable()
export class GlobalProvider {

    public newNotification: number = 0;
    static mapStyle = [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#444444"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#46bcec"
                },
                {
                    "visibility": "on"
                }
            ]
        }
    ];


    constructor(private alertCtrl: AlertController,
                public platform: Platform) {
    }

    showConfirm(title: string = 'Dies ist der Title', message: string = 'Dies ist die Message'): Promise<any> {
        return new Promise(resolve => {
            let confirm = this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [
                    {
                        text: 'Abbrechen',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Weiter',
                        handler: () => {
                            resolve(true);
                        }
                    },
                ]
            });
            confirm.present();
        })

    }

    showPromptWithCallback(message: string = 'Dies ist die Message', title: string = 'Erfolgreich', buttonText: string = 'Okay', enableBackDropDismiss: boolean = true) {
        return new Promise(resolve => {
            let confirm = this.alertCtrl.create({
                title: title,
                message: message,
                enableBackdropDismiss: enableBackDropDismiss,
                buttons: [
                    {
                        text: buttonText,
                        handler: () => {
                            resolve(true);
                        }
                    },
                ]
            });
            confirm.present();
        })
    }

    showPrompt(title: string = 'Dies ist der Title', message: string = 'Dies ist die Message') {
        let prompt = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                    }
                }
            ]

        });
        prompt.present();
    }

    checkOnlyNative(): Promise<any> {

        return new Promise((resolve, reject) => {
            if (
                this.platform.is('core') ||
                this.platform.is('mobileweb')
            ) {
                reject("Only Native");
            }
            resolve(true);
        });

    }

    showAlert(message: string, error: boolean = false) {

        console.log(message);
        const alert = this.alertCtrl.create({
            title: error ? "Fehler" : "Erfolgreich",
            subTitle: error ? message : message,
            buttons: ['OK']
        });
        alert.present();
    }

    showPromptInputWithCallback(
        title: string = 'Erfolgreich',
        message: string = 'Dies ist die Message',
        buttonText: string = 'Okay',
        enableBackDropDismiss: boolean = true
    ) {
        return new Promise(resolve => {

            let confirm = this.alertCtrl.create({
                title: title,
                message: message,
                enableBackdropDismiss: enableBackDropDismiss,
                inputs: [
                    {
                        placeholder: 'Notitz',
                        name: 'note',
                        type: 'text',
                    }
                ],
                buttons: [
                    {
                        text: 'Abbrechen',
                        handler: () => {
                        }
                    },
                    {
                        text: 'Weiter',
                        handler: (data) => {
                            resolve(data);
                        }
                    },
                ]
            });
            confirm.present();
        })
    }


    static currencyFormatDE(num) {
        return (
            num
                .toFixed(2)
                .replace('.', ',')
        )
    }

    static updateColor(color) {
        document.documentElement.style.setProperty(`--color`, color);
    }

    static favouriteSvg = '<?xml version="1.0" encoding="UTF-8"?>\n' +
        '<svg width="160px" height="160px" viewBox="0 0 160 160" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
        '<!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->\n' +
        '<title>Atoms / Icons / Favorit</title>\n' +
        '<desc>Created with Sketch.</desc>\n' +
        '<defs></defs>\n' +
        '<g id="Atoms-/-Icons-/-Favorit" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
        '    <path d="M105.42487,57.0640481 C102.768744,57.0795256 100.402283,55.2873679 99.5532701,52.6173946 L85.8715998,8.45187239 C85.0125896,5.78905302 82.6529974,4 80,4 C77.3470026,4 74.9874104,5.78905302 74.1284002,8.45187239 L60.4467299,52.6173946 C59.5977174,55.2873679 57.2312562,57.0795256 54.5751302,57.0640481 L10.1588659,57.0640481 C7.51983354,57.0012259 5.14972535,58.7686159 4.31188705,61.4241409 C3.47404875,64.0796658 4.36371915,67.0045173 6.50667463,68.639653 L42.5509928,96.1123423 C44.6806444,97.7168014 45.5797767,100.603693 44.7704014,103.238313 L31.0325435,147.600537 C30.4421342,149.579432 30.7851521,151.738811 31.9542903,153.40315 C33.1234285,155.067489 34.9759165,156.033547 36.9322371,155.999109 C38.2368095,155.992659 39.5057788,155.54706 40.5563346,154.726508 L76.3478088,127.450521 C78.5321133,125.793567 81.4678867,125.793567 83.6521912,127.450521 L119.443665,154.726508 C120.494221,155.54706 121.76319,155.992659 123.067763,155.999109 C125.024084,156.033547 126.876571,155.067489 128.04571,153.40315 C129.214848,151.738811 129.557866,149.579432 128.967456,147.600537 L115.229599,103.238313 C114.420223,100.603693 115.319356,97.7168014 117.449007,96.1123423 L153.493325,68.639653 C155.636281,67.0045173 156.525951,64.0796658 155.688113,61.4241409 C154.850275,58.7686159 152.480166,57.0012259 149.841134,57.0640481 L105.42487,57.0640481 Z" id="Star" fill="#2E88F4"></path>\n' +
        '</g>\n' +
        '</svg>'

}
