import {Vehicle} from "../../models/Vehicle";
import {Card} from "../../models/Card";
import {ProcessCategory} from "../../models/ProcessCategory";
import {Process} from "../../models/Process";
import {User_Token} from "../../models/User";
import {Dealer} from "../../models/Dealer";
import {NotificationModel} from "../../models/NotificationModel";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {Subject} from "rxjs/Subject";
import {ServiceProvider} from "../../models/ServiceProvider";

export class StorageProvider {

    static vehicle: Vehicle = null;
    static cards: Card[] = [];
    static processCategories: ProcessCategory[] = [];
    static activeProcessCategory: ProcessCategory = null;
    static notifications: NotificationModel[] = [];
    static processes: Process[] = [];
    static dealers: Dealer[] = [];
    static userToken: User_Token = null;
    static services: any[] = [];

    static favourites_sj = new ReplaySubject<any>(0);

    static menu: any[] = [];

    static activeProcess: Process = null;
    static activeSP: ServiceProvider = null;
    static form_id: number = 0;
    static activeDealerConnect: number = 0;
    static activeService: string = '';
    static processPolicy: string = '';
    static email: string = '';
    static password: string = '';
    static autoLogin: boolean = false;
    static eventSub: boolean = false;

    static activeCardByDealer: number = -1;

    static firstVisit: boolean = false;

    static readNotification: NotificationModel[] = [];

    static appVersion = new ReplaySubject<string>(0);
    static appOutDated = new BehaviorSubject<boolean>(false);

    constructor() {}

    static saveStorage(key, value) {
        let v: any = value;
        if (typeof value == "object") {
            v = JSON.stringify(value);
        }
        localStorage.setItem(key, v);
    }

    static getStorage(key) {
        let v: string = localStorage.getItem(key);
        if (this.isJsonString(v)) {
            return JSON.parse(v)
        }
        return v;
    }

    static removeStorage(key) {
        localStorage.removeItem(key);
    }

    static removeAllStorage() {
        localStorage.clear();
    }

    static updateStorage(key, value) {
        this.removeStorage(key);
        this.saveStorage(key, value);
    }

    static destroy(){
      this.vehicle = null;
      this.cards  = [];
      this.processCategories  = [];
      this.notifications  = [];
      this.processes  = [];
      this.dealers  = [];
      this.services  = [];
      this.favourites_sj = new ReplaySubject<any>(0);
      this.menu  = [];
      this.activeProcess  = null;
      this.activeDealerConnect  = 0;
      this.activeService  = '';
      this.processPolicy  = '';
      this.email = '';
      this.password  = '';
      this.autoLogin  = false;
      this.activeCardByDealer  = -1;
      this.firstVisit  = false;
      this.readNotification  = [];
      this.appVersion = new ReplaySubject<string>(0);
      this.appOutDated = new BehaviorSubject<boolean>(false);
    }

    static isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

}
