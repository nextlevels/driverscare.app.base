import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {GlobalProvider} from "../global/global";
import {ApiResponse} from "../../models/ApiResponse";
import {StorageProvider} from "../storage/storage";
import {_BASE_URL, _TEST_MODE, _TEST_MODE_SETTINGS} from "../../config";
import {HTTP} from "@ionic-native/http";
import {Platform} from "ionic-angular";
import {Observable} from 'rxjs/Observable';


@Injectable()
export class ApiProvider {

    constructor(private http: HttpClient,
                private global: GlobalProvider,
                private nativeHttp: HTTP,
                private platform: Platform
    ) {
    }

    login(email: string, password: string): Promise<ApiResponse> {
        let params: HttpParams = new HttpParams()
            .set("email", email)
            .set("password", password);

        return this.postHttp("/login", params);
    }

    getCars(): Promise<ApiResponse> {
        return this.getChoose("/user/vehicle");
    }

    getPoolCars(): Promise<ApiResponse> {
    return this.getChoose("/user/pool");
    }

    setPoolCar(id): Promise<ApiResponse> {
    return this.getChoose("/user/vehicle/add/"+id);
  }


    getCards(): Promise<ApiResponse> {
        return this.getChoose("/user/cards");
    }

    getServiceProviderForService(service_id: string, process_id: number): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "service_id": String(service_id),
                "process_id": String(process_id),
            }
        } else {
            params = new HttpParams()
                .set("process_id", process_id + "")
                .set("service_id", service_id);
        }
        return this.getChoose("/user/service-provider", params);
    }

    getServiceProviderForServiceGeo(service_id: string, process_id: number, lat: number, long: number): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "service_id": String(service_id),
                "lat": String(lat),
                "long": String(long),
                "process_id": String(process_id),
            }
        } else {
            params = new HttpParams()
                .set("process_id", process_id + "")
                .set("lat", lat + "")
                .set("long", long + "")
                .set("service_id", service_id);
        }
        return this.getChoose("/user/service-provider", params);
    }

    getAllServiceProvider(): Promise<ApiResponse> {
        return this.getChoose("/user/service-providers");
    }

    getDealersForService(params: any): Promise<ApiResponse> {

        let _params: any;
        if (this.platform.is("cordova")) {
            _params = {
                "service_id": String(params.service_id),
                "lat": String(params.lat),
                "long": String(params.long),
                "radius": String(params.radius),
                "process_id": String(params.process_id)
            };
            return this.getNativeHttp("/user/dealers", _params)
        } else {
            _params = new HttpParams()
                .set("service_id", params.service_id)
                .set("lat", params.lat)
                .set("process_id", params.process_id)
                .set("long", params.long)
                .set("radius", params.radius);
            return this.getHttp("/user/dealers", _params)
        }
    }

    getNotifications(): Promise<ApiResponse> {
        return this.getChoose("/user/notifications");
    }

    getAppVersion(): Promise<ApiResponse> {
        if (this.platform.is("cordova")) {
            return new Promise((resolve, reject) => {
                this.nativeHttp.get(_BASE_URL + '/appversion', null, {
                    'Access-Control-Allow-Origin': '*',
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }).then(data => {
                    resolve(<ApiResponse> JSON.parse(data.data));
                }, err => {
                    reject(JSON.parse(err.error));
                })
            });
        } else {
            return new Promise((resolve, reject) => {
                this.http.get(_BASE_URL + '/appversion', {
                    params: {},
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                }).subscribe(data => {
                    resolve(<ApiResponse> data);
                }, err => {
                    reject(err.error);
                });
            });
        }
    }

    addImages(images: any[]): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {
            let params: any = {};
            for (let i = 0; i < images.length; i++) {
                params['images[' + i + ']'] = images[i];
            }
            return this.postNativeHttp("/user/uploadimage", params)
        } else {
            let params: any = {};
            params = new HttpParams();
            for (let i = 0; i < images.length; i++) {
                params = params.set('images[' + i + ']', images[i]);
            }
            return this.postHttp("/user/uploadimage", params)
        }
    }

    uploadImage(image: any): Promise<ApiResponse> {
      if (this.platform.is("cordova")) {
        let params: any = {};
        params = {
          "image": image
        };
        return this.postNativeHttp("/user/uploadFormimage", params)
      } else {
        let params: any = {};
        params = new HttpParams()
          .set("image", image);
        return this.postHttp("/user/uploadFormimage", params)
      }
    }


    getDealerForService(service_id: string): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "service_id": service_id
            }
        } else {
            params = new HttpParams()
                .set("service_id", service_id);
        }
        return this.getChoose("/user/dealer", params);
    }

    getDealerSolutionForService(params: any): Promise<ApiResponse> {
        let _params: any;
        if (this.platform.is("cordova")) {
            _params = {
                "service_id": String(params.service_id),
            }
        } else {
            _params = new HttpParams()
                .set("service_id", params.service_id)
        }
        return this.getChoose("/user/dealer/solution", _params);
    }


    getCategories(): Promise<ApiResponse> {
        return this.getChoose((_TEST_MODE && _TEST_MODE_SETTINGS.all_processes) ? "/process-categories" : "/user/process-categories");
    }

    getProcess(id: number): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "process_id": id.toString()
            }
        } else {
            params = new HttpParams()
                .set("process_id", id.toString());
        }
        return this.getChoose("/user/process", params);
    }

    getFavorites(): Promise<ApiResponse> {
        return this.getChoose("/user/favorites");
    }


    getChargeDetail(station: number): Promise<any> {


        if (this.platform.is("cordova")) {

            return new Promise((resolve, reject) => {
                this.nativeHttp.get("https://api.enbw.com/emobility-public/api/v1/chargestations/" + station, {}, {
                    "Ocp-Apim-Subscription-Key": "bd155d66715f4629af837c20ce31377f"
                }).then(data => {
                    resolve(JSON.parse(data.data));
                }, err => {
                    reject(err);
                });
            });
        } else {
            return new Promise((resolve, reject) => {
                this.http.get("https://api.enbw.com/emobility-public/api/v1/chargestations/" + station, {
                    headers: {
                        "Ocp-Apim-Subscription-Key": "bd155d66715f4629af837c20ce31377f"
                    }
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    reject(err);
                });
            });
        }
    }


    getChargeStations(fromLat: string, toLat: string, fromLon: string, toLon: string, authenticationMethods: string = ""): Promise<any> {
        if (this.platform.is("cordova")) {
            let params: any = {
                "fromLat": fromLat,
                "toLat": toLat,
                "fromLon": fromLon,
                "toLon": toLon,
                "grouping": "true",
                "groupingDivisor": "15"
            };

            if (authenticationMethods)
                params["authenticationMethods"] = authenticationMethods;

            return new Promise((resolve, reject) => {
                this.nativeHttp.get("https://api.enbw.com/emobility-public/api/v1/chargestations", params, {
                    "Ocp-Apim-Subscription-Key": "bd155d66715f4629af837c20ce31377f"
                }).then(data => {
                    resolve(JSON.parse(data.data));
                }, err => {
                    reject(err);
                });
            });
        } else {
            return new Promise((resolve, reject) => {

                let params: any = {
                    "fromLat": fromLat,
                    "toLat": toLat,
                    "fromLon": fromLon,
                    "toLon": toLon,
                    "grouping": "true",
                    "groupingDivisor": "15"
                };
                if (authenticationMethods)
                    params["authenticationMethods"] = authenticationMethods;

                this.http.get("https://api.enbw.com/emobility-public/api/v1/chargestations", {
                    params: params,
                    headers: {
                        "Ocp-Apim-Subscription-Key": "bd155d66715f4629af837c20ce31377f"
                    }
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    reject(err);
                });
            });
        }
    }

    setCardLostOrFound(id: string, note: string = ''): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "card_id": id.toString(),
                "lost_note": note.toString(),
                "lost_at": Date.now().toString()
            }

        } else {
            params = new HttpParams()
                .set("card_id", id.toString())
                .set("lost_note", note.toString())
                .set("lost_at", Date.now().toString());
        }
        return this.postChoose("/user/cards/notify-loss", params);

    }

    addFavorites(id: number): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "process_id": id.toString()
            }

        } else {
            params = new HttpParams()
                .set("process_id", id.toString());
        }
        return this.getChoose("/user/favorites/add", params);
    }

    addDealer(id: number, places: string, service_id: string): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "dealer_id": id.toString(),
                "places_id": places,
                "service_id": service_id
            }

        } else {
            params = new HttpParams()
                .set("dealer_id", id.toString())
                .set("places_id", places)
                .set("service_id", service_id);
        }
        return this.getChoose("/user/dealer/add", params);
    }

    getSvgImage(image: string): Promise<any> {

        return new Promise((resolve, reject) => {
            this.nativeHttp.get(image, {}, {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/text',
                'Content-Type': 'application/text'
            }).then(data => {
                resolve(data.data);
            }, err => {
                reject(JSON.parse(err.error));
            })
        });

    }

    deleteDealer(id: number, service_id: string): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "dealer_id": id.toString(),
                "service_id": service_id
            }

        } else {
            params = new HttpParams()
                .set("dealer_id", id.toString())
                .set("service_id", service_id)
        }
        return this.getChoose("/user/dealer/remove", params);
    }


    addCard(params: any): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {

            let paramss = {
                "card_color": params['card_color'],
                "code_number": params['code_number'],
                "code_type": params['code_type'],
                "name": params['name'],
                "service_provider_id": params['service_provider_id']
            };

            if (params['front_image'])
                paramss["front_image"] = params['front_image'];

            if (params['back_image'])
                paramss["back_image"] = params['back_image'];

            return this.postNativeHttp("/user/cards/add", paramss);
        } else {
            let paramss: HttpParams = new HttpParams()
                .set("card_color", params['card_color'])
                .set("code_number", params['code_number'])
                .set("code_type", params['code_type'])
                .set("name", params['name'])
                .set("service_provider_id", params['service_provider_id']);

            if (params['front_image'])
                paramss = paramss.append('front_image', params['front_image']);

            if (params['back_image'])
                paramss = paramss.append('back_image', params['back_image']);

            return this.postHttp("/user/cards/add", paramss);
        }
    }


    updateCard(params: any): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {

            let paramss = {
                "card_id": params['card_id'],
                "card_color": params['card_color'],
                "code_number": params['code_number'],
                "code_type": params['code_type'],
                "name": params['name'],
                "service_provider_id": params['service_provider_id']
            };

            if (params['front_image'])
                paramss["front_image"] = params['front_image'];

            if (params['back_image'])
                paramss["back_image"] = params['back_image'];

            return this.postNativeHttp("/user/cards/update", paramss);
        } else {
            let paramss: HttpParams = new HttpParams()
                .set("card_id", params['card_id'])
                .set("card_color", params['card_color'])
                .set("code_number", params['code_number'])
                .set("code_type", params['code_type'])
                .set("name", params['name'])
                .set("service_provider_id", params['service_provider_id']);

            if (params['front_image'])
                paramss = paramss.append('front_image', params['front_image']);

            if (params['back_image'])
                paramss = paramss.append('back_image', params['back_image']);

            return this.postHttp("/user/cards/update", paramss);
        }
    }

    sendPosition(params: any): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {

            let paramss = {
                'dealer_name': String(params['dealer_name']),
                'dealer_email': String(params['dealer_email']),
                'lat': String(params['lat']),
                'long': String(params['long']),
            };

            return this.postNativeHttp("/user/cards/add", paramss);
        } else {
            let paramss: HttpParams = new HttpParams()
                .set("dealer_name", params['dealer_name'])
                .set("dealer_email", params['dealer_email'])
                .set("lat", params['lat'])
                .set("long", params['long']);

            return this.postHttp("/user/send-position", paramss);
        }
    }

    removeCard(id: string): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {
            let params = {
                card_id: id
            };
            return this.postNativeHttp("/user/cards/remove", params);
        } else {
            let params: HttpParams = new HttpParams()
                .set("card_id", id);
            return this.postHttp("/user/cards/remove", params);
        }

    }

    removeFavorites(id: number): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "process_id": id.toString()
            }
        } else {
            params = new HttpParams()
                .set("process_id", id.toString());
        }
        return this.getChoose("/user/favorites/remove", params);
    }

    book(data: any): Promise<ApiResponse> {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "send_vehicle_information": data.send_vehicle_information,
                "process_id": data.process_id,
                "service_id": data.service_id,
                "dealer_name": data.dealer_name,
                "dealer_email": data.dealer_email,
            };
            for (let i = 0; i < data.appointments.length; i++) {
                params["appointments[" + i + "]"] = data.appointments[i];
            }
        } else {
            params = new HttpParams()
                .set("send_vehicle_information", data.send_vehicle_information)
                .set("process_id", data.process_id)
                .set("service_id", data.service_id)
                .set("dealer_name", data.dealer_name)
                .set("dealer_email", data.dealer_email);

            for (let i = 0; i < data.appointments.length; i++) {
                params = params.append("appointments[" + i + "]", data.appointments[i]);
            }
        }
        return this.postChoose("/user/book", params);
    }

    confirmBook(id: any) {
        let params: any;
        if (this.platform.is("cordova")) {
            params = {
                "appointment_id": id,
            };
        } else {
            params = new HttpParams()
                .set("appointment_id", id)
        }
        return this.postChoose("/user/book/accept", params);
    }

    updateData(model: string, field: string, value: string): Promise<ApiResponse> {

        if (this.platform.is("cordova")) {
            let params: any = {
                "model": model,
                "field": field,
                "value": value
            };
            return this.postNativeHttp("/user/process/model", params);
        } else {
            let params: HttpParams = new HttpParams()
                .set("model", model)
                .set("field", field)
                .set("value", value);
            return this.postHttp("/user/process/model", params);
        }
    }

    setToken(s_token: string, device_type: number = 0, reset_token: number = 0): Promise<ApiResponse> {
        let params: HttpParams = new HttpParams()
            .set("token", s_token)
            .set("device_type", device_type.toString())
            .set("reset_token", reset_token.toString());
        return this.getHttp("/user/token", params);
    }

    private getChoose(endpoint: string, params: any = null): Promise<any> {
        if (this.platform.is("cordova")) {
            return this.getNativeHttp(endpoint, params);
        } else {
            return this.getHttp(endpoint, params);
        }
    }

    private postChoose(endpoint: string, params: any = null): Promise<any> {
        if (this.platform.is("cordova")) {
            return this.postNativeHttp(endpoint, params);
        } else {
            return this.postHttp(endpoint, params);
        }
    }

    private getHttp(endpoint: string, params: HttpParams = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.http.get(_BASE_URL + endpoint, {
                params: params,
                headers: this.headers()
            }).subscribe(data => {
                resolve(<ApiResponse> data);
            }, err => {
                reject(err.error);
            });
        });
    }

    private postHttp(endpoint: string, params: HttpParams = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.http.post(_BASE_URL + endpoint, params, {
                headers: this.headers().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            }).subscribe(data => {
                resolve(<ApiResponse> data);
            }, err => {
                reject(<string> err.error);
            });
        });
    }

    private getNativeHttp(endpoint: string, params: any = null, absoluteEndpoint: boolean = false): Promise<ApiResponse> {

        let url = _BASE_URL + endpoint;
        if (absoluteEndpoint) {
            url = endpoint;
        }

        return new Promise((resolve, reject) => {
            this.nativeHttp.get(url, params, {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization": "bearer " + StorageProvider.userToken.token,
            }).then(data => {
                resolve(<ApiResponse> JSON.parse(data.data));
            }, err => {
                reject(JSON.parse(err.error));
            })
        });
    }

    private postNativeHttp(endpoint: string, params: any = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.nativeHttp.post(_BASE_URL + endpoint, params, {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                "Authorization": "bearer " + StorageProvider.userToken.token,
            }).then(data => {
                resolve(<ApiResponse> JSON.parse(data.data));
            }, err => {
                reject(<string> JSON.parse(err.error));
            });
        });
    }

    private postTranserNativeHttp(endpoint: string, params: any = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.nativeHttp.post(_BASE_URL + endpoint, params, {
                'Content-Type': 'application/form-data; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Authorization': 'bearer ' + StorageProvider.userToken.token,
            }).then(data => {
                resolve(<ApiResponse> JSON.parse(data.data));
            }, err => {
                reject(<string> err.error);
            });
        });
    }

    postWithUpload(endpoint: string, params: any = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.nativeHttp.post(_BASE_URL + endpoint, params, {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                "Authorization": "bearer " + StorageProvider.userToken.token,
            }).then(data => {
                resolve(<ApiResponse> JSON.parse(data.data));
            }, err => {
                reject(<string> err.error);
            });
        });
    }

    private postTransferHttp(endpoint: string, params: HttpParams = null): Promise<ApiResponse> {
        return new Promise((resolve, reject) => {
            this.http.post(_BASE_URL + endpoint, params, {
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Authorization": "bearer " + StorageProvider.userToken.token
                }
            }).subscribe(data => {
                alert(JSON.stringify(data));
                resolve(<ApiResponse> data);
            }, err => {
                alert(JSON.stringify(err));
                reject(<string> err.error);
            });
        });
    }

    private headers() {
        let headers: HttpHeaders = new HttpHeaders();

        if (StorageProvider.userToken) {
            headers = new HttpHeaders()
                .set('Access-Control-Allow-Origin', '*')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .set("Authorization", "bearer " + StorageProvider.userToken.token);
        }
        return headers;
    }

    getNearbyPlaces(lat: number, long: number, radius: string, type: string, keyword: string): any {

        if (this.platform.is("cordova")) {
            let params: any = {
                "location": lat + ',' + long,
                "radius": radius,
                "type": type,
                "keyword": keyword,
                "key": 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI',
            };
            return this.getNativeHttp('https://maps.googleapis.com/maps/api/place/nearbysearch/json', params, true);
        } else {
            let params: HttpParams = new HttpParams()
                .set("location", lat + ',' + long)
                .set("radius", radius)
                .set("type", type)
                .set("keyword", keyword)
                .set("key", 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI');

            return new Promise((resolve, reject) => {
                this.http.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json', {
                    params: params
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    console.log(err);
                });
            });
        }
    }

    getAdressCoords(searchField: string) {

        if (this.platform.is("cordova")) {
            let params: any = {
                address: searchField,
                key: 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI'
            };
            return this.getNativeHttp('https://maps.googleapis.com/maps/api/geocode/json', params, true);
        } else {
            let params: HttpParams = new HttpParams()
                .set("address", searchField)
                .set("key", 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI');

            return new Promise((resolve, reject) => {
                this.http.get('https://maps.googleapis.com/maps/api/geocode/json', {
                    params: params
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    console.log(err);
                });
            });
        }
    }

    getNearbyPlacesDetails(placeID: string): any {

        if (this.platform.is("cordova")) {
            let params: any = {
                "placeid": placeID,
                "fields": 'name,rating,formatted_phone_number,website',
                "key": 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI',
            };
            return this.getNativeHttp('https://maps.googleapis.com/maps/api/place/details/json', params, true);
        } else {
            let params: HttpParams = new HttpParams()
                .set("placeid", placeID)
                .set("fields", 'name,rating,formatted_phone_number,website')
                .set("key", 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI');

            return new Promise((resolve, reject) => {
                this.http.get('https://maps.googleapis.com/maps/api/place/details/json', {
                    params: params
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    console.log(err);
                });
            });
        }
    }

    getNearbyPlacesDetailsFull(placeID: string): any {

        if (this.platform.is("cordova")) {
            let params: any = {
                "placeid": placeID,
                "fields": 'name,rating,formatted_phone_number,website,url,photo,formatted_address,vicinity',
                "key": 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI',
            };
            return this.getNativeHttp('https://maps.googleapis.com/maps/api/place/details/json', params, true);
        } else {
            let params: HttpParams = new HttpParams()
                .set("placeid", placeID)
                .set("fields", 'name,rating,formatted_phone_number,website,url,photo,formatted_address,vicinity')
                .set("key", 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI');

            return new Promise((resolve, reject) => {
                this.http.get('https://maps.googleapis.com/maps/api/place/details/json', {
                    params: params
                }).subscribe(data => {
                    resolve(<any> data);
                }, err => {
                    console.log(err);
                });
            });
        }
    }

    getLatLongAdress(adress: string): any {
        let params: HttpParams = new HttpParams()
            .set("address", adress)
            .set("key", 'AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI');

        return new Promise((resolve, reject) => {
            this.http.get(' https://maps.googleapis.com/maps/api/geocode/json', {
                params: params
            }).subscribe(data => {
                resolve(<any> data);
            }, err => {
                console.log(err);
            });
        });
    }

    getStaticPage(page: string): Observable<any> {
        return this.http.get("./assets/static-content/" + page + ".json", {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        });
    }

  postData(process_id: number, form_data: any[], form_data_id: number = 0): Promise<ApiResponse> {
    let params: any;
    if (this.platform.is("cordova")) {
      params = {
        "process_id": process_id.toString()
      }

      for(let data in form_data){
       params["form_data["+data+"]"] = form_data[data] ;
      }

      if(form_data_id != 0){
        params["form_data_id"] = form_data_id.toString() ;
      }


    } else {
      params = new HttpParams()
        .set("process_id", process_id.toString());

      for(let data in form_data){
        params = params.append("form_data["+data+"]", form_data[data]);
      }

      if(form_data_id != 0){
        params = params.set("form_data_id", form_data_id.toString());
      }
    }


    return this.postChoose("/user/process/save-form-data", params);
  }

  sendData(form_data_id: number, mail_template_id: string, target_email: string): Promise<ApiResponse> {
    let params: any;
    if (this.platform.is("cordova")) {
      params = {
        "form_data_id": form_data_id.toString(),
        "mail_template_id": mail_template_id,
        "target_email": target_email
      }

    } else {
       params = new HttpParams()
        .set("form_data_id", form_data_id.toString())
        .set("mail_template_id", mail_template_id)
        .set("target_email", target_email);
    }

    return this.postChoose("/user/process/send-form-data", params);
  }

  trackState(id: number, type:string): Promise<ApiResponse> {
    let params: any;
    if (this.platform.is("cordova")) {
      params = {
        "process_id": id.toString(),
        "type": type
      }

    } else {
      params = new HttpParams()
        .set("process_id", id.toString())
        .set("type", type);
    }
    return this.getChoose("/user/process/track-end", params);
  }

  getResellerFields(id: number): Promise<ApiResponse> {
    let params: any;
    if (this.platform.is("cordova")) {
      params = {
        "process_id": id.toString()
      }

    } else {
      params = new HttpParams()
        .set("process_id", id.toString())
    }
    return this.getChoose("/user/process/reseller-fields", params);
  }

}
