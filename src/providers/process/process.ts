import {Injectable} from '@angular/core';
import {AlertController, Events, NavController, Platform} from "ionic-angular";
import {Process} from "../../models/Process";
import {ApiProvider} from "../api/api";
import {StorageProvider} from "../storage/storage";
import {GlobalProvider} from "../global/global";
import {ProcessStep} from "../../models/ProcessStep";
import {MaplistPage} from "../../pages/maplist/maplist";
import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {DealerinfoPage} from "../../pages/dealerinfo/dealerinfo";
import {Dealer} from "../../models/Dealer";
import {BookinghelperPage} from "../../pages/bookinghelper/bookinghelper";
import {FormprocessPage} from "../../pages/formprocess/formprocess";
import {UpdateDataProcessPage} from "../../pages/update-data-process/update-data-process";
import {MenuListProcessPage} from "../../pages/menu-list-process/menu-list-process";
import {MaplistEnbwPage} from "../../pages/maplist-enbw/maplist-enbw";
import {ShowInfoProcessPage} from "../../pages/show-info-process/show-info-process";
import {Geolocation} from "@ionic-native/geolocation";
import {ServiceProvider} from "../../models/ServiceProvider";
import {InputprocessPage} from "../../pages/inputprocess/inputprocess";
import {HomePage} from "../../pages/home/home";
import {WalletPage} from "../../pages/wallet/wallet";

@Injectable()
export class ProcessProvider {

    activeProcess: Process;

    constructor(private alertCtrl: AlertController,
                private platform: Platform,
                private api: ApiProvider,
                private global: GlobalProvider,
                private events: Events,
                private callNumber: CallNumber,
                private iab: InAppBrowser,
                private geo: Geolocation
    ) {
    }

    handle(process: Process) {
      if(process.fields && process.fields.length > 0){
        if(process.companies[0].pivot.fields && process.companies[0].pivot.fields.length > 0){
          this.pushProcess(this.fillProcessWithFields(process,process.companies[0].pivot.fields));
        }else{
          this.api.getResellerFields(process.id)
            .then(data => {
              if(data.response != null){
                this.pushProcess(this.fillProcessWithFields(process,data.response));
              }else{
                this.pushProcess(process);
              }
            })
        }
      }else {
        this.pushProcess(process);
      }
    }

    pushProcess(process: Process){
      if(process.display_name!==""){
        process.name = process.display_name;
      }

      StorageProvider.activeProcess = process;
      StorageProvider.activeDealerConnect = 0;
      StorageProvider.form_id = 0;

      if (process.process_steps.length > 0)
        this.nextAction(process.process_steps.find(x => x.initial == true));

      if (process.companies && process.companies[0].pivot.policy) {
        StorageProvider.processPolicy = process.companies[0].pivot.policy;
      } else {
        StorageProvider.processPolicy = '';
      }
    }

    getNextStep(processStepId: number) {
        return StorageProvider.activeProcess.process_steps.filter(x => x.id == processStepId)[0];
    }

    nextAction(processStep: ProcessStep) {
        if(processStep===null || processStep===undefined){
          this.events.publish('page:change', HomePage, false, true);
        }

        switch (processStep.name) {
            case "QuestionAlert": {

                break;
            }

            case "DealerList": {
                let service_id = processStep.process_inputs.filter(x => x.field_name == "Service")[0].field_value;
                this.getMapsList(service_id);
                break;
            }

            case "ShowDealer": {
                let service_id = processStep.process_inputs.filter(x => x.field_name == "Service")[0].field_value;
                this.getDealerInfo(service_id);
                break;
            }

            case "ChoiseDealer": {
                let service_id = processStep.process_inputs.filter(x => x.field_name == "Service")[0].field_value;
                this.getDealerChoise(service_id);
                break;
            }

            case "Enbw": {
                let googleList: any = processStep.process_inputs.filter(x => x.field_name == "GoogleList");
                if (googleList[0]) {
                    googleList = googleList[0].field_value;
                }

                this.getChargeStations();
                break;
            }

            case "ShowCard": {
                break;
            }

            case "CallPhone": {
                let Link = processStep.process_inputs.filter(x => x.field_name == "Nummer")[0].field_value;
                this.callPhone(Link);
                break;
            }

            case "CallLink": {
                let Link = processStep.process_inputs.filter(x => x.field_name == "Link")[0].field_value;
                this.callLink(Link);
                break;
            }

            case "CallApp": {
                this.openApp(this.objectifyProcessFields(processStep.process_inputs));
                break;
            }

            case "UpdateData": {
                this.updateData(processStep);
                break;
            }

            case "Input": {

                  this.openInputProcess(processStep);

                break;
            }

            case "MenuList": {
                this.openMenuList(processStep);
                break;
            }

            case "ShowInfo": {
                this.openShowInfo(processStep);
                break;
            }

            case "Form": {
                this.openForm(processStep);
                break;
            }

          case "FormSelect": {
            this.openInputProcess(processStep);
            break;
          }

          case "FormPictures": {
            this.openInputProcess(processStep);
            break;
          }

          case "FormTextarea": {
            this.openInputProcess(processStep);
            break;
          }

          case "FormInputs": {
            this.openInputProcess(processStep);
            break;
          }


          case "SendData": {
            this.sendData(processStep);
            break;
          }

            default: {
                this.events.publish('page:change', HomePage);
                break;
            }
        }

    }

    callLink(site: string) {
      if(site=="Karten"){
        this.events.publish('page:change', WalletPage);
        return;
      }

      if (!this.platform.is('cordova')) {
        return;
      }
        if (this.platform.is('ios')) {
          window.open(site, '_system');
        }
      if (this.platform.is('android')) {
        const browser = this.iab.create(site, '_blank');
        browser.on('loadstop').subscribe(event => {
          browser.insertCSS({code: "body{color: red;"});
        });
      }
    }

    openForm(processStep: ProcessStep) {
        this.events.publish('page:change', FormprocessPage);
    }

    updateData(processStep: ProcessStep) {
        this.events.publish('page:change', UpdateDataProcessPage, this.objectifyProcessFields(processStep.process_inputs));
    }

   sendData(processStep: ProcessStep) {

     this.api.trackState(StorageProvider.activeProcess.id,'finish').then(data => { console.log(data);})
     let target_mail = processStep.process_inputs.filter(x => x.field_name == "Zielemail")[0].field_value;
     let mail_template = processStep.process_inputs.filter(x => x.field_name == "Mail Template")[0].field_value;
     this.api.sendData(StorageProvider.form_id,mail_template,target_mail)
       .then(data => {
         StorageProvider.form_id = 0;
         console.log(processStep.process_outputs);
         if(processStep.process_outputs  === undefined || processStep.process_outputs.length == 0){
           this.events.publish('page:change', HomePage);
         }else{
           this.nextAction(this.getNextStep((processStep.process_outputs[0].process_input as any).process_step_id))

         }
       }, error => {
         this.global.showAlert(JSON.stringify(error), true);
       });
  }

    openMenuList(processStep: ProcessStep) {
        this.events.publish('page:change', MenuListProcessPage, this.objectifyProcessFieldss(processStep));
    }

    openInputProcess(processStep: ProcessStep) {
      let step = this.objectifyProcessFieldss(processStep);
      (step as any).typename = processStep.name;
      this.events.publish('page:change', InputprocessPage,step);
    }

    openShowInfo(processStep: ProcessStep) {
        this.events.publish('page:change', ShowInfoProcessPage, this.objectifyProcessFieldss(processStep));
    }

    callPhone(number: string) {
        if (!this.platform.is('cordova')) {
            this.global.showPrompt('Fehler', 'Diese Funktion wird nur auf einem nativen Endgerät unterstützt.');
            return;
        }
        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    getMapsList(service_id: any, service_provider: ServiceProvider = <ServiceProvider>null) {
      StorageProvider.activeService = service_id;
        let params = {
            service_id: service_id
        };
        if (service_provider) {
            params['service_provider'] = service_provider
        }
        this.events.publish('page:change', MaplistPage, params);
    }

    getChargeStations() {
        const params = {
            type: "enbw",
            data: ''
        };
        this.events.publish('page:change', MaplistEnbwPage, params);

    }

    getDealerInfo(service_id: string) {
        this.getDealerOrMapList(service_id);
    }

    getDealerChoise(service_id: string) {
        this.getDealerOrMapList(service_id);
    }

     getDealerOrMapList(service_id : string){
        StorageProvider.activeService = service_id;
        this.api.getServiceProviderForService(service_id,StorageProvider.activeProcess.id)
            .then(data => {
                let resp: ServiceProvider = <ServiceProvider>data.response;
                 StorageProvider.activeSP = resp;
                if (resp.is_dealer_instance) {
                    resp.dealer_instance["file_banner"] = resp.file_banner;
                    this.events.publish('page:change', DealerinfoPage, {
                        dealer_with_instance: resp,
                    });
                } else {
                    this.api.getDealerSolutionForService({
                        'service_id': service_id
                    }).then(data => {

                        if (data.response === null) {
                            StorageProvider.activeDealerConnect = 1;
                            this.getMapsList(service_id, resp);
                        } else {
                            StorageProvider.activeDealerConnect = 2;
                            this.events.publish('page:change', DealerinfoPage, {
                                dealer: data.response,
                            });
                        }

                    }, error => {
                        this.global.showPrompt('Fehler', error.errorMessage.toString());
                    })
                }

            }, error => {
                this.global.showPrompt('Fehler', error.errorMessage.toString());
            });
    }

    openApp(process_inputs: any) {
        if (!this.platform.is('cordova')) {
            this.global.showPrompt('Fehler', 'Diese Funktion wird nur auf einem nativen Endgerät unterstützt.');
            return;
        }
        if (this.platform.is('ios')) {
            if (process_inputs.iosLink) {
                try {
                    window["plugins"].launcher.canLaunch({uri: process_inputs.iosLink}, () => {
                        window["plugins"].launcher.launch({uri: process_inputs.iosLink});
                    }, (err) => {
                        if (process_inputs.iosLink) {
                            window.open(process_inputs.iosStore, '_system');
                        }
                    });
                }
                catch (e) {
                    window.open(process_inputs.iosStore, '_system');
                }

            }

        }
        else if (this.platform.is('android')) {
            if (process_inputs.androidLink) {
                window["plugins"].launcher.canLaunch({packageName: process_inputs.androidLink}, () => {
                    window["plugins"].launcher.launch({packageName: process_inputs.androidLink});
                }, () => {
                    if (process_inputs.androidStore) {
                        window.open(process_inputs.androidStore, '_system');
                    }
                });
            }
        }
    }

    navigateNativeMap(destination: string) {
        if (this.platform.is('ios')) {
            window.open('maps://?q=' + destination, '_system');
        }
        else if (this.platform.is('android')) {
            let label = encodeURI('');
            window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
        } else {
            this.global.showPrompt('Diese Funktion wird nur auf einem nativen Endgerät unterstützt.');
        }
    }

    bookDate(dealer: Dealer) {
        this.events.publish('page:change', BookinghelperPage, dealer);
    }

    objectifyProcessFields(process_inputs: any[]) {
        let object = {};
        for (let entry of process_inputs) {
            object[entry['field_name']] = entry['field_value']
        }
        return object;
    }


    objectifyProcessFieldss(processStep: any) {
        let object = {};
        if (processStep.process_inputs && processStep.process_inputs.length != 0) {
            for (let entry of processStep.process_inputs) {
                object[entry['field_name']] = entry['field_value']
            }
        }
        if (processStep.process_outputs && processStep.process_outputs.length != 0) {
            object['outputs'] = processStep.process_outputs;
        }
        return object;
    }

    fillProcessWithFields(process: any, fields:any){
      let processTemp = process;

      if(this.IsJsonString(fields)){
        for (let field of JSON.parse(fields)) {
          let step = processTemp.process_steps.filter(x => x.id == field['step_id']);
          if(step !== 'undefined' && step[0] != undefined){
            step[0].process_inputs.filter(x => x.field_name == field['field'])[0].field_value = field['value'];
          }
        }
      }
      return processTemp;
    }

   IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
}
