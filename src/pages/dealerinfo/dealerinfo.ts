import {Component} from '@angular/core';
import {ActionSheetController, Events, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {Dealer} from "../../models/Dealer";
import {Process} from "../../models/Process";
import {StorageProvider} from "../../providers/storage/storage";
import {
    styler,
    tween,
    easing
} from 'popmotion';
import {ApiProvider} from "../../providers/api/api";
import {ProcessProvider} from "../../providers/process/process";
import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {AppAvailability} from "@ionic-native/app-availability";
import {BookinghelperPage} from "../bookinghelper/bookinghelper";
import {ServiceProvider} from "../../models/ServiceProvider";
import {TabsPage} from "../tabs/tabs";
import {WalletPage} from "../wallet/wallet";
import {GlobalProvider} from "../../providers/global/global";
import {Geolocation} from "@ionic-native/geolocation";
import {BookingIframePage} from "../booking-iframe/booking-iframe";

@Component({
    selector: 'page-dealerinfo',
    templateUrl: 'dealerinfo.html',
})
export class DealerinfoPage {

    dealer: Dealer;
    service_provider: ServiceProvider;
    activeProcess: Process;
    processProvider: ProcessProvider;
    dealerConnectState: number;

    action = {
        type: '',
        label: '',
        url: '',
        show: true
    };

    constructor(
        public navCtrl: NavController,
        public platform: Platform,
        private api: ApiProvider,
        private events: Events,
        private navParams: NavParams,
        private callNumber: CallNumber,
        private iab: InAppBrowser,
        private appAvailibility: AppAvailability,
        private actionSheet: ActionSheetController,
        private global: GlobalProvider,
        private geo: Geolocation
    ) {

        this.dealer = this.navParams.get('dealer');

        if (this.navParams.get('param')) {
            if (this.navParams.get('param').dealer) {
                this.dealer = this.navParams.get('param').dealer
            }
            this.service_provider = this.navParams.get('param').dealer_with_instance;
            if (this.service_provider) {
                this.dealer = <any>this.service_provider.dealer_instance
            }
        }

        if (this.navParams.get('service_provider')) {
            this.service_provider = this.navParams.get('service_provider');
        }

        let serviceProvider: ServiceProvider;

        if (this.dealer && this.dealer.service_provider && this.service_provider && !this.service_provider.is_dealer_instance) {
            serviceProvider = this.dealer.service_provider;
        } else if (this.service_provider) {
            serviceProvider = this.service_provider;
        }
        if (!serviceProvider) {
            serviceProvider = this.dealer.service_provider;
        }

        if (serviceProvider) {

            if (typeof (this.dealer.email) === 'undefined') {
                let email: string = "info@";
                if (this.dealer.website) {
                    const hostname = new URL(this.dealer.website).hostname;
                    if (hostname.includes('www')) {
                        email += hostname.substr(4);
                    } else {
                        email += hostname;
                    }
                }
                this.dealer.email = email;
            }

            switch (serviceProvider.action_type) {
                case 'booking':
                    this.action.type = 'book';
                    this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Termin buchen';
                    this.action.show = Boolean(this.dealer.website || this.dealer.email);
                    break;
                case 'card':
                    this.action.type = 'card';
                    this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Zur Karte';
                    break;
                case 'url':
                    this.action.type = 'url';
                    this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Zum Link';
                    this.action.url = serviceProvider.action_url;
                    break;
                case 'position':
                    this.action.type = 'position';
                    this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Standort senden';
                    this.action.url = serviceProvider.action_url;
                    break;
                case 'atu':
                  this.action.type = 'atu';
                  this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Termin buchen';
                  this.action.url = serviceProvider.action_url;
                  break;
                default:
                    this.action.type = 'book';
                    this.action.label = serviceProvider.action_label ? serviceProvider.action_label : 'Termin buchen';
                    this.action.show = Boolean(this.dealer.website || this.dealer.email);
                    break
            }
        }


        if (this.dealer) {
            if (this.dealer.places_id != null) {
                if (this.dealer.id != null) {
                    this.api.getNearbyPlacesDetailsFull(this.dealer.places_id).then(data => {
                        if (data) {
                            if (data.result) {
                                let picture = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' + (data.result.photos ? data.result.photos[0].photo_reference : '') + '&key=AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI';
                                let pictureObject = new Object();
                                pictureObject['path'] = picture;

                                this.dealer.telephone = data.result.formatted_phone_number;
                                this.dealer.website = data.result.website;
                                this.dealer.name = data.result.name;
                                this.dealer.street = data.result.vicinity;


                                if(this.dealer.service_provider.file_banner){

                                }else {
                                  this.dealer.file_banner = pictureObject;
                                }
                                this.dealer.places_id = data.result.place_id;
                            }
                        }
                    }, error => {
                    })
                } else {
                    this.dealer.id = 0;
                }

            } else {
                this.dealer.places_id = '';
            }

        }

        this.activeProcess = StorageProvider.activeProcess;
        this.service_provider = {...serviceProvider};
    }

    doAction(action: any) {
        switch (action.type) {
            case 'atu':
              this.events.publish('page:change', BookingIframePage, {param: this.dealer});
              break;
            case 'book':
              this.bookDate();
              break;
            case 'card':
                if (this.service_provider) {
                    StorageProvider.activeCardByDealer = this.service_provider.id;
                    const findCard = StorageProvider.cards.find(x => x['service_provider_id'] === this.service_provider.id);
                    if (!findCard) {
                        this.global.showAlert('Karte nicht gefunden', true);
                        break;
                    }

                }
                this.events.publish('tab:page', {
                    tab: 2
                });
                break;
            case 'url':
                console.log(action.url);
                this.actionLink(action.url);
                break;
        }
    }

    ionViewDidLoad() {
        const element = document.querySelector('page-dealerinfo .dasboard-wrapper');

        tween({
            from: {height: 0 + 'vh'},
            to: {height: (58 + 'vh')},
            ease: easing.backOut,
            duration: 1500
        }).start(styler(element).set);
    }


    navigateTo(destination: string) {
        this.appAvailibility.check('maps://')
            .then((yes: boolean) => {

            }, (no: boolean) => {

            });

        if (this.platform.is('ios')) {
            this.appAvailibility.check('comgooglemaps://')
                .then((yes: boolean) => {
                    this.presentActionSheet(destination);
                }, (no: boolean) => {
                    window.open('maps://?q=' + destination, '_system');
                });
        } else {
            let label = encodeURI('');
            window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
        }
    }

    presentActionSheet(destination: string) {
        const actionSheet = this.actionSheet.create({
            title: 'Mit welcher App möchten Sie navigieren?',
            buttons: [
                {
                    text: 'Google Maps',
                    handler: () => {
                        let label = encodeURI('');
                        window.open('comgooglemaps://?q=' + destination + '(' + label + ')', '_system');
                    }
                }, {
                    text: 'Karten',
                    handler: () => {
                        window.open('maps://?q=' + destination, '_system');
                    }
                }, {
                    text: 'Zurück',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }


    actionCall(number: string) {
      this.api.trackState(this.activeProcess.id,'call').then(data => {})

        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    actionLink(link: string) {
      this.api.trackState(this.activeProcess.id,'link').then(data => {})
      if (!this.platform.is('cordova')) {
        return;
      }
      if (this.platform.is('ios')) {
        window.open(link, '_system');
      }
      if (this.platform.is('android')) {
        const browser = this.iab.create(link, '_blank');
        browser.on('loadstop').subscribe(event => {
          browser.insertCSS({code: "body{color: red;"});
        });
      }
    }

    bookDate() {
      this.api.trackState(this.activeProcess.id,'book').then(data => {})
        if (this.dealer.service_provider) {
            if (this.dealer.service_provider.booking_url) {
                this.actionLink(this.dealer.service_provider.booking_url);
            } else {
                this.events.publish('page:change', BookinghelperPage, {param: this.dealer});
            }
        } else {
            this.events.publish('page:change', BookinghelperPage, {param: this.dealer});
        }
    }

    sendPositon() {
      this.api.trackState(this.activeProcess.id,'position').then(data => {})
        if (this.platform.is("cordova")) {

            this.geo.getCurrentPosition()
                .then(data => {
                    let params = {
                        'dealer_name': this.dealer.name,
                        'dealer_email': this.dealer.email,
                        'lat': data.coords.latitude,
                        'long': data.coords.longitude,
                    };

                    this.api.sendPosition(params).then(data => {
                        this.global.showAlert('Standort erfolgreich gesendet');
                    }, error => {
                        this.global.showAlert('Irgendwas ist schiefgegangen',true);
                    })
                });
        } else {
            let params = {
                'dealer_name': this.dealer.name,
                'dealer_email': this.dealer.email,
                'lat': "52.520008",
                'long': "13.404954",
            };

            this.api.sendPosition(params).then(data => {
                this.global.showAlert('Standort erfolgreich gesendet');
            }, error => {
                this.global.showAlert('Irgendwas ist schiefgegangen',true);
            })
        }
    }

    addDealer() {
        StorageProvider.activeDealerConnect = 2;
        this.api.addDealer(this.dealer.id, this.dealer.places_id, StorageProvider.activeService)
            .then(data => {

            }, error => {

            })
    }

    deleteDealer() {
        StorageProvider.activeDealerConnect = 1;
        this.api.deleteDealer(this.dealer.id, StorageProvider.activeService)
            .then(data => {

            }, error => {

            })
    }

    getButtonState() {
        return StorageProvider.activeDealerConnect;
    }

}
