import {Component} from '@angular/core';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ViewController} from "ionic-angular";


@Component({
    selector: 'page-splash-animation',
    templateUrl: 'splash-animation.html',
})
export class SplashAnimationPage {

    constructor(public splashScreen: SplashScreen,
                public viewCtrl: ViewController) {
    }

    ionViewDidEnter() {
        this.splashScreen.hide();
        setTimeout(() => {
            this.viewCtrl.dismiss();
        }, 2000);
    }
}
