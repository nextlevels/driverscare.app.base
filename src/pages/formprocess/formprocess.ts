import {Component} from '@angular/core';
import {LoadingController, NavController, NavParams, normalizeURL} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {GlobalProvider} from "../../providers/global/global";
import {ImagePicker, ImagePickerOptions} from "@ionic-native/image-picker";
import {ApiProvider} from "../../providers/api/api";

@Component({
    selector: 'page-formprocess',
    templateUrl: 'formprocess.html',
})
export class FormprocessPage {

    imageURI: any;
    images: string[] = [];

    constructor(public navParams: NavParams,
                public navCtrl: NavController,
                private camera: Camera,
                public global: GlobalProvider,
                private imagePicker: ImagePicker,
                private api: ApiProvider,
                private loadingCtrl: LoadingController
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FormprocessPage');
    }

    pickImages() {

        this.images = [];

        let options = {
            maximumImagesCount: 3,
            outputType: 1
        };
        this.imagePicker.getPictures(options).then((results) => {
            for (let i = 0; i < results.length; i++) {
                this.images.push(this.mod(results[i]));
            }
        }, (err) => {
            console.log("error: " + err);
        });

    }

    getImage() {
        const options: CameraOptions = {
            quality: 60,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true
        };

        this.camera.getPicture(options).then((imageData) => {
            this.images.push(this.mod(imageData));
        }, (err) => {
            console.log(err);
        });
    }

    uploadFile() {
        let loading = this.loadingCtrl.create({
            spinner: 'circles',
            content: 'Hochladen...'
        });
        loading.present();
        this.api.addImages(this.images).then(data => {
            this.global.showAlert('Erfolgreich hochgeladen', false);
            this.images = [];
            loading.dismiss();
        }, error => {
            this.global.showAlert('Hochgeladen fehlgeschlagen', true);
            loading.dismiss();
        });
    }

    removeFile(i: number) {
        this.images.splice(i, 1);
    }

    mod(url: string) {
        return 'data:image/jpeg;base64,' + url;
    }

}
