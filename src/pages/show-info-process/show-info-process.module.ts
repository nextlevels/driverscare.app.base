import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowInfoProcessPage } from './show-info-process';

@NgModule({
  declarations: [
      ShowInfoProcessPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowInfoProcessPage),
  ],
})
export class ShowInfoProcessPageModule {}
