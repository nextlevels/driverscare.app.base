import {Component, Injector} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProcessProvider} from "../../providers/process/process";
import {ProcessOutput} from "../../models/ProcessOutput";

@IonicPage()
@Component({
    selector: 'page-show-info-process',
    templateUrl: 'show-info-process.html',
})
export class ShowInfoProcessPage {

    processFields: any;
    title: string;
    text: string;
    outputs: ProcessOutput[];
    private processProvider;

    constructor(private navParams: NavParams, injector:Injector) {
        setTimeout(() => this.processProvider = injector.get(ProcessProvider))
    }


    ionViewDidLoad() {
        this.processFields = this.navParams.get('param');
        this.title = this.processFields.Title;
        this.text = this.processFields.Text;
        this.outputs = this.processFields.outputs;
    }

    chooseOption(process_input:any){
      if(process_input == null){
        this.processProvider.nextAction(null);
      }else{
        this.processProvider.nextAction(this.processProvider.getNextStep(process_input.process_step_id));
      }
    }

}
