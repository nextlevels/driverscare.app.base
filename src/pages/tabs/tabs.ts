import {Component, OnInit, ViewChild} from '@angular/core';

import {AboutPage} from '../about/about';
import {HomePage} from '../home/home';
import {MorePage} from "../more/more";
import {WalletPage} from "../wallet/wallet";
import {NotificationsPage} from "../notifications/notifications";
import {GlobalProvider} from "../../providers/global/global";
import {Events, Tabs} from "ionic-angular";

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit{

    @ViewChild('tabs') tabRef: Tabs;

    tab1Root = HomePage;
    tab2Root = AboutPage;
    tab3Root = WalletPage;
    tab4Root = NotificationsPage;
    tab5Root = MorePage;

    card : any;

    constructor(
        private global: GlobalProvider,
        private events : Events
    ) {

    }
    ngOnInit(){
        this.events.subscribe('tab:page', (params) => {
            try {
                this.tabRef.select(params.tab);
            } catch (ex) {
            }
        });
    }
}
