import {Component, HostListener} from '@angular/core';
import {App, Events, ModalController, NavController, Platform} from 'ionic-angular';
import {StorageProvider} from "../../providers/storage/storage";
import {ProcessCategory} from "../../models/ProcessCategory";
import {Process} from "../../models/Process";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {AllProcessesPage} from "../all-processes/all-processes";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ProcessProvider} from "../../providers/process/process";
import {OneSignal} from "@ionic-native/onesignal";
import * as $ from 'jquery';
import {DomSanitizer} from "@angular/platform-browser";
import {ProfilePage} from "../profile/profile";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    processCategories: ProcessCategory[] = [];
    processes: Process[] = [];
    carName: string = '';
    carImg: string = '';
    activeCategory: boolean[] = [];
    wait: boolean = false;

    processList: Process[] = [];
    cleanList: Process[] = [];

    favorites : any[] = [];
    look : boolean = false;

    pushToken: string = "";

    constructor(public navCtrl: NavController,
                public platform: Platform,
                private api: ApiProvider,
                private events: Events,
                private iab: InAppBrowser,
                private global: GlobalProvider,
                private process: ProcessProvider,
                private oneSignal: OneSignal,
                private sanitizer: DomSanitizer,
                private modalCtrl: ModalController,
                public app: App
    ) {

      if(!StorageProvider.eventSub){
        events.subscribe('page:change', (page, data = null, root = false) => {
          if(root){
            this.app.getActiveNav().setRoot(page);
          }else{
            if (data) {
              this.app.getActiveNav().push(page, {param: data});
            } else {
              this.app.getActiveNav().push(page);
            }
          }
        });
        StorageProvider.eventSub = true;
      }

      this.initializeItems();

      setTimeout(() => {
        $('.category-image').each((index, img) => {
          let $img = $(img);
          let imgClass = $img.attr('class');
          let imgURL = $img.attr('src');
          if (this.platform.is("cordova")) {
            this.api.getSvgImage(imgURL)
              .then(data => {
                this.replaceImgToSvg(data, imgClass, $img, true);
              })
          } else {
            $.get(imgURL, (data) => {
              this.replaceImgToSvg(data, imgClass, $img, false);
            }, 'xml');
          }
        });
      }, 100);

      if (this.platform.is("cordova")) {
        this.oneSignal.startInit('69805c4d-d0e6-4b92-bfb6-69eba9d971ba', '455593382435');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        });

        this.oneSignal.handleNotificationOpened().subscribe((data) => {
        });

        this.oneSignal.setSubscription(true);
        this.oneSignal.endInit();
        this.oneSignal.getIds()
          .then(data => {
            this.pushToken = data.pushToken;
            this.api.setToken(this.pushToken, this.platform.is('android') ? 1 : 0);
          });
      }

      if (StorageProvider.processCategories) {
        this.processCategories = StorageProvider.processCategories;
        StorageProvider.favourites_sj.subscribe(data => {
          const hiddenFav = data.data.length === 0;
          if (StorageProvider.firstVisit === false) {
            this.processCategories.unshift({
              name: 'Favoriten',
              processes: data.data,
              category_icon: {path: 'https://s3.eu-central-1.amazonaws.com/drivers-care.com/uploads/public/Favorit.svg'},
              hidden: hiddenFav
            } as ProcessCategory);
            StorageProvider.firstVisit = true;
            if(!hiddenFav){
              this.changeCategory(this.processCategories[0]);
            }
          }else{
            this.processCategories[0].hidden = hiddenFav;
          }
          if(hiddenFav){
            this.changeCategory(this.processCategories[1]);
          }

          if (this.processCategories[0].name === 'Favoriten' && !hiddenFav) {
            this.processCategories[0].processes = data.data;

            if(StorageProvider.activeProcessCategory){
              this.changeCategory(StorageProvider.activeProcessCategory);
            }else {
              this.changeCategory(this.processCategories[0]);
            }
          }
        });
      }


      this.platform.ready().then(() => {
        this.api.getNotifications().then(data => {
          if (StorageProvider.getStorage('readNotification')) {
            let keysLength = StorageProvider.getStorage('readNotification').length;
            let notificationLength = data.response.length;
            let result = notificationLength - keysLength;
            this.global.newNotification = result > 0 ? result : 0;
          }
        });

        this.api.getCards()
          .then(data => {
            StorageProvider.cards = data.response;
            StorageProvider.saveStorage("cards", data.response);
          }, error => {
            this.global.showAlert(JSON.stringify(error), true);
          });

      });
    }

  ionViewWillEnter() {
    this.look = false;
    if (StorageProvider.vehicle != null)
      this.carName = StorageProvider.vehicle.license_plate;
    if (StorageProvider.vehicle.company.app_logo) {
      this.carImg = StorageProvider.vehicle.company.app_logo['path'];
    }else if(StorageProvider.vehicle.company.reseller.app_logo){
      this.carImg = StorageProvider.vehicle.company.reseller.app_logo['path'];
    }
    }

    changeCategory(processCategory: ProcessCategory) {
        this.activeCategory = [];
       if(processCategory.id == undefined){
         processCategory.id = 0;
       }
        StorageProvider.activeProcessCategory = processCategory;
        this.activeCategory[processCategory.id] = true;
        this.processes = processCategory.processes.sort(function (a, b) {

          if(a.companies[0] && b.companies[0]){
            if(a.companies[0].pivot.sort == b.companies[0].pivot.sort)
            {
              return ((a.display_name.toLowerCase() > b.display_name.toLowerCase()) ? 1 : -1);
            }
            else
            {
              return (a.companies[0].pivot.sort < b.companies[0].pivot.sort) ? -1 : 1;
            }
          }else {
            return ((a.display_name.toLowerCase() > b.display_name.toLowerCase()) ? 1 : -1);
          }

        });
    }

    doAction(process: Process) {
      console.log(this.look);
      if(!this.look){
        this.look = true;
        this.api.getProcess(process.id)
          .then(data => {
            this.process.handle(process);
            this.look = false;
          })
      }
    }

    navigateAllProcesses() {
      this.app.getActiveNav().push(AllProcessesPage);
    }

    replaceImgToSvg(data, imgClass, $img, native) {
        let $svg;
        if (native) {
            $svg = $(data)
        } else {
            $svg = $(data).find("svg");
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }
        $svg.attr("height", "30px");
        $svg.attr("width", "30px");
        // Replace image with new SVG
        $img.replaceWith($svg);
    }

  initializeItems() {
    StorageProvider.processCategories.forEach((value, index) => {
      if (value.name != 'Favoriten') {
        this.processList.push.apply(this.processList, value.processes);
      }
    });
    this.cleanList = this.processList.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1);;
  }

  getItems(ev: any) {
    this.processList = this.cleanList;
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.processList = this.processList.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    this.processes = this.processList;
  }

  openProfile() {
    this.navCtrl.push(ProfilePage);
  }

  openProcesses() {
    this.navCtrl.push(AllProcessesPage);
  }

}

