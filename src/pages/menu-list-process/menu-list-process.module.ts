import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuListProcessPage } from './menu-list-process';
import {InfoModalComponent} from "../../components/info-modal/info-modal";

@NgModule({
  declarations: [
    MenuListProcessPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuListProcessPage)
  ],
})
export class MenuListProcessPageModule {}
