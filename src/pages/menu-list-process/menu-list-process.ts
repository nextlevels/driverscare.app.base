import {Component, Injector} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProcessProvider} from "../../providers/process/process";
import {ProcessOutput} from "../../models/ProcessOutput";
import {StorageProvider} from "../../providers/storage/storage";

@IonicPage()
@Component({
    selector: 'page-menu-list-process',
    templateUrl: 'menu-list-process.html',
})
export class MenuListProcessPage {

    processFields: any;
    title: string;
    description: string;
    outputs: ProcessOutput[];
    private processProvider;
    processName: string;

    constructor(private navParams: NavParams, injector:Injector) {
        setTimeout(() => this.processProvider = injector.get(ProcessProvider))
    }


    ionViewDidLoad() {
        this.processName = StorageProvider.activeProcess.name;
        this.processFields = this.navParams.get('param');
        this.title = this.processFields.Title;
        this.description = this.processFields.Description;
        this.outputs = this.processFields.outputs;
    }

    chooseOption(process_input:any){
        this.processProvider.nextAction(this.processProvider.getNextStep(process_input.process_step_id));
    }

}
