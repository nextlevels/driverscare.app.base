import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Camera} from '@ionic-native/camera';
import {GlobalProvider} from "../../providers/global/global";
import {ImagePicker} from "@ionic-native/image-picker";
import {ApiProvider} from "../../providers/api/api";
import {StorageProvider} from "../../providers/storage/storage";
import {TabsPage} from "../tabs/tabs";


@Component({
    selector: 'page-update-data-process',
    templateUrl: 'update-data-process.html',
})
export class UpdateDataProcessPage {

   processFields: any;
   question: string;
   processName: string;
   label: string;
   inputValue : string;

    constructor(public navParams: NavParams,
                public navCtrl: NavController,
                private camera: Camera,
                public global: GlobalProvider,
                private imagePicker: ImagePicker,
                private api: ApiProvider) {
    }

    ionViewDidLoad() {
        this.processFields = this.navParams.get('param');
        this.processName = StorageProvider.activeProcess.name;
        this.question = this.processFields.Frage;
        this.label = this.processFields.Label;


    }

    updateData() {
      this.api.trackState(StorageProvider.activeProcess.id,'update').then(data => {})
        this.api.updateData(this.processFields.Model,this.processFields.Field,this.inputValue)
            .then(data => {
                this.global.showAlert('Erfolgreich versendet');
                this.navCtrl.push(TabsPage);
            }, error => {
                this.global.showAlert(error, true);
            })
    }

}
