import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Process} from "../../models/Process";
import {StorageProvider} from "../../providers/storage/storage";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {ProcessProvider} from "../../providers/process/process";

@Component({
    selector: 'page-all-processes',
    templateUrl: 'all-processes.html',
})
export class AllProcessesPage {

    processList: Process[] = [];
    cleanList: Process[] = [];
    searchQuery: string = '';
    wait: boolean = false;

    favourites: Process[] = [];

    constructor(private navCtrl: NavController,
                private navParams: NavParams,
                private api: ApiProvider,
                private global: GlobalProvider,
                private process: ProcessProvider) {
        this.initializeItems();
    }

    ionViewDidLoad() {
        StorageProvider.favourites_sj.subscribe(data => {
            this.favourites = data.data;
            StorageProvider.saveStorage("favorites", this.favourites);
        })
    }

    initializeItems() {
        StorageProvider.processCategories.forEach((value, index) => {
            if (value.name != 'Favoriten') {
                this.processList.push.apply(this.processList, value.processes);
            }
        });
        this.cleanList = this.processList.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : -1);;
    }

    getItems(ev: any) {
        this.processList = this.cleanList;
        const val = ev.target.value;
        if (val && val.trim() != '') {
            this.processList = this.processList.filter((item) => {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

    toogleFavorite(process: Process) {

        if (this.favourites.length == 0 || !this.favourites.some(x => x.id == process.id)) {

            this.api.addFavorites(process.id)
                .then(data => {
                    this.favourites.push(process);
                    StorageProvider.favourites_sj.next({
                        toggle: true,
                        data: this.favourites
                    });
                }, error => {
                    this.global.showAlert(error.errorMessage, true);
                }).then(() => {
            })

        } else {
            this.api.removeFavorites(process.id)
                .then(data => {
                    StorageProvider.favourites_sj.next({
                            toggle: true,
                            data: this.favourites.filter(x => x.id !== process.id)
                        }
                    );
                }, error => {
                    this.global.showAlert(error, true);
                }).then(() => {
            });
        }
    }

    isFavorite(process: Process) {
        if (this.favourites.length != 0 && this.favourites.some(x => x.id === process.id)) {
            return 'ios-star';
        } else {
            return 'ios-star-outline';
        }
    }

    doAction(process: Process) {
        console.log(process);
        this.wait = true;
        setTimeout(() => {
            this.wait = false;
        }, 2000);
        this.process.handle(process);
    }
}
