import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, Events, PickerOptions} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {StaticContentPage} from '../static-content/static-content';
import {StorageProvider} from "../../providers/storage/storage";
import {AllProcessesPage} from "../all-processes/all-processes";
import {ProfilePage} from "../profile/profile";
import {LanguageService} from "../../providers/language.service";
import {TranslateService} from "@ngx-translate/core";
import {PickerController} from "ionic-angular";

@Component({
    selector: 'page-more',
    templateUrl: 'more.html',
})
export class MorePage {

    language = '';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private modalCtrl: ModalController,
        private events: Events,
        private translate: TranslateService,
        private pickerCtrl: PickerController
    ) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad MorePage');
    }

    logout() {
      StorageProvider.destroy();
      //his.events.unsubscribe('page:change');
      localStorage.clear();
      LoginPage.userAuth.next(null);
    }

    openStaticPage(page: string) {
        this.modalCtrl.create(StaticContentPage, {data: page}).present();
    }

  openProfile() {
    this.navCtrl.push(ProfilePage);
  }

  openProcesses() {
    this.navCtrl.push(AllProcessesPage);
  }

  async showPicker() {
      console.log(this.translate.instant('ABOUT.title'));
      let opts: PickerOptions = {
        buttons: [
          {
            text: this.translate.instant('MORE.cancel'),
            role: 'cancel',
          },
          {
            text: this.translate.instant('MORE.done')
          }
        ],
        columns: [
          {
            name: 'languages',
            options: [
              { text: this.translate.instant('MORE.english'), value: 'en'},
              { text: this.translate.instant('MORE.german'), value: 'de'},
            ]
          }
        ]
      };
      let picker = await this.pickerCtrl.create(opts);
      picker.present();

      picker.onDidDismiss(async data =>{
        let col = await picker.getColumn('languages');
        this.language = col.options[col.selectedIndex].value;
        this.translate.use(this.language);
      });

  }
}
