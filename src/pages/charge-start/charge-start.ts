import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {ChargeSimulationPage} from "../charge-simulation/charge-simulation";

@Component({
    selector: 'page-charge-start',
    templateUrl: 'charge-start.html',
})
export class ChargeStartPage {

    charger: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private modalCtrl : ModalController
    ) {

        this.charger = this.navParams.data
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ChargeStartPage');
    }

    loadChargeStart() {
        this.modalCtrl.create(ChargeSimulationPage).present();
    }

}
