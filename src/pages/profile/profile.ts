import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {User} from "../../models/User";
import {StorageProvider} from "../../providers/storage/storage";
import {Vehicle} from "../../models/Vehicle";


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user : User;
  vehicle : Vehicle;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user = StorageProvider.userToken.user;
    this.vehicle = StorageProvider.vehicle;
    console.log(this.user);

  }

  ionViewDidLoad() {

  }

}
