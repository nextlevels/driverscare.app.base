import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {GlobalProvider} from "../../providers/global/global";
import {StorageProvider} from "../../providers/storage/storage";
import {Vehicle} from "../../models/Vehicle";
import * as moment from "moment";
import {SocialSharing} from "@ionic-native/social-sharing";
import {el} from "@angular/platform-browser/testing/src/browser_util";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'page-info-modal',
    templateUrl: 'info-modal.html',
})
export class InfoModalPage {

    processPolicy:any = '';
    kfzinfo:string = 'Info';
    vehicle : Vehicle;
    mileAge : number = 0;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private socialSharing: SocialSharing,
                private global: GlobalProvider,
                private sanitizer:DomSanitizer) {
      this.vehicle = StorageProvider.vehicle;
      this.vehicle =  this.navParams.get('car');
    }

    ionViewDidLoad() {
      this.processPolicy = this.sanitizer.bypassSecurityTrustHtml(StorageProvider.processPolicy)
      this.kfzinfo = this.navParams.get('kfzinfo');

    if(this.vehicle){
      this.mileAge = this.vehicle.mileage;

    }
    }

  getDate(date : any){
    if(!date)
      return '';
    return moment(date, "YYYY.MM.DD HH:mm:ss").format('MM/YYYY');
  }

  share(){
    let shareContent = "";
    let subject = "Fahrzeugschein"
    switch (this.kfzinfo) {
        case "Fahrzeugschein":

          if(this.vehicle.registration_document != null){
            shareContent = this.vehicle.registration_document.path;
            this.socialSharing.share('',null,this.vehicle.registration_document.path,null);
            return;
          }else{
            shareContent = "" +
              "Kennzeichen: " + this.vehicle.license_plate + "\n" +
              "Marke: " + this.vehicle.brand + "\n" +
              "Typ: " + this.vehicle.type + "\n" +
              "Model: " + this.vehicle.model + "\n" +
              "Kraftstoffart: " + this.vehicle.fuel_type + "\n" +
              "Schadstoffklasse: " + this.vehicle.emissions_class + "\n" +
              "kW/PS: " + this.vehicle.kw_hp + "\n" +
              "Erstzulassung: " + this.vehicle.first_registration + "\n" +
              "FIN: " + this.vehicle.fin + "\n" +
              "Winterreifen: " + this.vehicle.wheel_size_winter + "\n" +
              "Sommerreifen: " + this.vehicle.wheel_size_summer + "\n";

          }


           subject = "Fahrzeugschein";
          break;
      case "Versicherung":
        if(this.vehicle.insurance_card != null){
          shareContent = this.vehicle.insurance_card.path;
          this.socialSharing.share('',null,this.vehicle.insurance_card.path,null);
          return;
        }else {
          shareContent = "" +
            "Versicherungspartner: " + this.vehicle.insurance + "\n" +
            "Versicherungsnummer: " + this.vehicle.insurance_number + "\n" +
            "Teilkasko: " + this.vehicle.insurance_number + "\n" +
            "Vollkasko: " + this.vehicle.insurance_number + "\n";
        }
        subject = "Versicherung";
        break;
      case "Verträge":
        shareContent = "" +
          "Leasing Partner: " + this.vehicle.lessor.name + "\n" +
          "Leasing Beginn: " + this.vehicle.leasing_start + "\n" +
          "Leasing Ende: " + this.getDate(this.vehicle.leasing_end) + "\n";
        subject = "Verträge";
        break;
      case "Car-Policy":
        shareContent = this.vehicle.company.policy;
        subject = "Car-Policy";
        break;
      }



    this.socialSharing.share(shareContent,subject).then(data => {
    },error => {
      this.global.showAlert(error,true);
    })
  }
    close() {
        this.navCtrl.pop();
    }

}
