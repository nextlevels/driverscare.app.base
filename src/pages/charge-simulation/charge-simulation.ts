import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

@Component({
    selector: 'page-charge-simulation',
    templateUrl: 'charge-simulation.html',
})
export class ChargeSimulationPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ChargeSimulationPage');
    }

    close() {
        this.navCtrl.pop();
    }

}
