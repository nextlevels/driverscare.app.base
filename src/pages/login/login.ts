import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {StorageProvider} from "../../providers/storage/storage";
import {ApiProvider} from "../../providers/api/api";
import {TabsPage} from "../tabs/tabs";
import {GlobalProvider} from "../../providers/global/global";
import {_APP_VERSION, _TEST_MODE, _TEST_MODE_SETTINGS, _TEST_USER} from "../../config";
import {User_Token} from "../../models/User";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    private todo: FormGroup;
    private clickedOnLogin: boolean = false;
    private appIsOutdated: boolean = false;
    private toggleSaveLogin: boolean = true;

    static userAuth = new ReplaySubject<User_Token>(0);

    registerCredentials = {
        email: (_TEST_MODE && _TEST_MODE_SETTINGS.test_user_auto_fill) ? _TEST_USER.email : '',
        password: (_TEST_MODE && _TEST_MODE_SETTINGS.test_user_auto_fill) ? _TEST_USER.password : '',
        autoLogin: false,
    };

    constructor(private formBuilder: FormBuilder,
                private storage: StorageProvider,
                private navCtrl: NavController,
                private api: ApiProvider,
                private global: GlobalProvider,
                private iab: InAppBrowser,
    ) {
        this.todo = this.formBuilder.group({
            email: [this.registerCredentials.email, Validators.required],
            password: [this.registerCredentials.password, Validators.required],
        });

        if (_TEST_MODE && _TEST_MODE_SETTINGS.auto_login) {
            this.logForm();
        }

        if (StorageProvider.getStorage("email") && StorageProvider.getStorage("password")) {
            this.registerCredentials.email = StorageProvider.getStorage("email");
            this.registerCredentials.password = StorageProvider.getStorage("password");

            if(StorageProvider.getStorage("autoLogin")){
                this.logForm();
            }
        }

    }

    ionViewDidLoad() {

    }

    ionViewDidEnter() {
        this.api.getAppVersion()
            .then(data => {
                StorageProvider.appVersion.next(data.response.app_version);
            }, error => {

            });

        StorageProvider.appOutDated.subscribe(isOutDated => {
            this.appIsOutdated = isOutDated
        });

        LoginPage.userAuth.subscribe(data => {
            if (data) {

                StorageProvider.userToken = data;
                StorageProvider.saveStorage("userToken", data);

              this.api.getCars()
                .then(data => {
                  StorageProvider.vehicle = data.response;
                  console.log(StorageProvider.vehicle.company.app_color);
                  if (StorageProvider.vehicle.company.app_color) {
                    let color = StorageProvider.vehicle.company.app_color;
                    GlobalProvider.updateColor(color);
                  }
                  StorageProvider.saveStorage("vehicle", data.response);

                  this.api.getCategories()
                    .then(data => {
                      StorageProvider.processCategories = data.response;
                      StorageProvider.saveStorage("processCategories", data.response);
                    }, error => {
                      this.global.showAlert(error['errorMessage'],true);
                    })
                    .then(() => {
                      this.api.getFavorites()
                        .then(data => {
                          console.log(data);
                          if (data.success) {
                            StorageProvider.favourites_sj.next({
                              toggle: false,
                              data: data.response
                            });
                            StorageProvider.saveStorage("favorites", data.response);
                          }
                        }, error => {
                          this.global.showAlert(error, true);
                        })
                        .then(() => {

                          this.navCtrl.setRoot(TabsPage).then((data) => {

                          })
                        })
                    });

                }, error => {
                  this.global.showAlert('Ihnen wurde kein Fahrzeug oder keine Fahrzeuggruppe zugeordnet. Bitte wenden Sie sich an Ihren Flottenmanager. ');
                })





              this.api.getNotifications().then(data => {

                if (StorageProvider.getStorage('readNotification')) {
                  let keysLength = StorageProvider.getStorage('readNotification').length;
                  let notificationLength = data.response.length;

                  let result = notificationLength - keysLength;
                  this.global.newNotification = result > 0 ? result : 0;
                }

              });

              this.api.getCards()
                .then(data => {
                  StorageProvider.cards = data.response;
                  StorageProvider.saveStorage("cards", data.response);
                }, error => {
                  this.global.showAlert(JSON.stringify(error), true);
                });

              this.api.getCars()
                .then(data => {
                  StorageProvider.vehicle = data.response;
                  console.log(StorageProvider.vehicle.company.app_color);
                  if (StorageProvider.vehicle.company.app_color) {
                    let color = StorageProvider.vehicle.company.app_color;
                    GlobalProvider.updateColor(color);
                  }
                  StorageProvider.saveStorage("vehicle", data.response);


                }, error => {
                  this.global.showAlert(JSON.stringify(error), true);
                })

            } else {
                if (StorageProvider.userToken) {
                    StorageProvider.userToken = null;
                    StorageProvider.removeStorage('userToken');
                    StorageProvider.removeStorage('autoLogin');
                    this.navCtrl.push(LoginPage);
                    LoginPage.userAuth.observers[0].complete();
                }
            }
        })
    }


    logForm() {
        this.clickedOnLogin = true;
        this.api.login(this.registerCredentials.email, this.registerCredentials.password)
            .then(data => {

                const isActive: boolean = data.response.user.is_activated;
                if (!isActive) {
                    this.global.showAlert("Der Benutzer ist noch nicht aktiviert", true);
                    this.clickedOnLogin = false;
                    return;
                }

                StorageProvider.email = this.registerCredentials.email;
                StorageProvider.password = this.registerCredentials.password;



                if (this.toggleSaveLogin) {
                    StorageProvider.saveStorage("autoLogin", this.toggleSaveLogin);
                    StorageProvider.saveStorage("email", this.registerCredentials.email);
                    StorageProvider.saveStorage("password", this.registerCredentials.password);
                }

                LoginPage.userAuth.next(data.response);

            }, error => {
                this.clickedOnLogin = false;
                console.log(error.errorMessage);
                this.global.showAlert(error.errorMessage, true);
            });

    }

    toggleLogin(ev) {
        this.toggleSaveLogin = ev.value;
    }

    forgotPassword() {
        const browser = this.iab.create('https://drivers-care.com/new-password', '_blank');
        browser.on('loadstop').subscribe(event => {
            browser.insertCSS({code: "body{color: red;"});
        });
    }

}
