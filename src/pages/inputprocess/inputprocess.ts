import {Component, Injector} from '@angular/core';
import {LoadingController, NavController, NavParams, normalizeURL} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {GlobalProvider} from "../../providers/global/global";
import {ImagePicker, ImagePickerOptions} from "@ionic-native/image-picker";
import {ApiProvider} from "../../providers/api/api";
import {ProcessOutput} from "../../models/ProcessOutput";
import {ProcessProvider} from "../../providers/process/process";
import {StorageProvider} from "../../providers/storage/storage";

@Component({
    selector: 'page-inputprocess',
    templateUrl: 'inputprocess.html',
})
export class InputprocessPage {

  processFields: any;
  title: string;
  description: string;
  outputs: ProcessOutput[];
  options: any[];
  name: string;
  label: string = 'Kamera öffnen';
  type: string;
  processStepType: string;
  private processProvider;
  imageURI: any;
  images: string[] = [];
  formvalues: any[] = [];

  processName: string;
  mandatory: string;

  constructor(public navParams: NavParams,
                public navCtrl: NavController,
                private camera: Camera,
                public global: GlobalProvider,
                private imagePicker: ImagePicker,
                private api: ApiProvider,
                private loadingCtrl: LoadingController,
              injector:Injector
    ) {
    setTimeout(() => this.processProvider = injector.get(ProcessProvider))
    }


  ionViewWillEnter() {

    if(StorageProvider.activeProcess.companies[0].pivot.custom_name != null){
      this.processName = StorageProvider.activeProcess.companies[0].pivot.custom_name;
    }else{
      this.processName =  StorageProvider.activeProcess.name;
    }

    this.processFields = this.navParams.get('param');
    this.processStepType =  this.processFields.typename;
    this.title = this.processFields.Seitentitel;
    this.name = this.processFields.Feldname;
    if(this.processFields.Feldbezeichnung && this.processFields.Feldbezeichnung != ''){
      this.label = this.processFields.Feldbezeichnung;
    }

    this.mandatory = this.processFields['Pflicht oder nicht?'];
     this.description = this.processFields.Frage;
    this.outputs = this.processFields.outputs;

    if(this.processFields['Einzel oder Mehrfachauswahl']=='Einzel'){
      this.type = "radio";
    }
    if(this.processFields['Einzel oder Mehrfachauswahl']=='Mehrfach'){
      this.type = "checkbox";
    }

    if(this.processFields.Optionen){
      this.options = this.processFields.Optionen.split(',');
    }

    if(this.processFields.Elemente){
      this.options = this.processFields.Elemente.split(',');
    }
  }

  chooseOption(process_input:any){
    if(this.formvalues  != null  && Object.keys(this.formvalues).length > 0){
      console.log(StorageProvider.form_id);
    this.api.postData(StorageProvider.activeProcess.id,this.formvalues,StorageProvider.form_id)
      .then(data => {
        StorageProvider.form_id = data.response.id;
        console.log(StorageProvider.form_id);
        this.processProvider.nextAction(this.processProvider.getNextStep(process_input.process_step_id));
      }, error => {
        this.global.showAlert(JSON.stringify(error), true);
      });
    }else{
      if(this.mandatory == 'Ja'){
        this.global.showAlert('Daten unvollständig', true);
      }else{
        this.processProvider.nextAction(this.processProvider.getNextStep(process_input.process_step_id));
      }
    }
  }

  pickImages() {

    this.images = [];

    let options = {
      maximumImagesCount: 1,
      outputType: 1
    };
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Hochladen...'
    });
    loading.present();

    this.imagePicker.getPictures(options).then((results) => {
      if(results.length > 0){
        for (let i = 0; i < results.length; i++) {
          this.api.uploadImage(this.mod(results[i])).then(data => {
            loading.dismiss();
            this.images.push(this.mod(results[i]));
            this.formvalues[this.name] = data.response;
          }, error => {
            loading.dismiss();
            this.global.showAlert('Hochgeladen fehlgeschlagen', true);
          });
        }
      }else{
        loading.dismiss();
      }

    }, (err) => {
      console.log("error: " + err);
    });

  }

  getImage() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: false
    };

    this.camera.getPicture(options).then((imageData) => {
      let loading = this.loadingCtrl.create({
        spinner: 'circles',
        content: 'Hochladen...'
      });
      loading.present();
      this.api.uploadImage(this.mod(imageData)).then(data => {
        loading.dismiss();
        this.images.push(this.mod(imageData));
      this.formvalues[this.name] = data.response;
      }, error => {
        this.global.showAlert('Hochgeladen fehlgeschlagen', true);
      });
    }, (err) => {
      console.log(err);
    });
  }

  uploadFile() {
    let loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Hochladen...'
    });
    loading.present();
    this.api.addImages(this.images).then(data => {
      this.global.showAlert('Erfolgreich hochgeladen', false);
      this.images = [];
      loading.dismiss();
    }, error => {
      this.global.showAlert('Hochgeladen fehlgeschlagen', true);
      loading.dismiss();
    });
  }

  removeFile(i: number) {
    this.images.splice(i, 1);
  }

  mod(url: string) {
    return 'data:image/jpeg;base64,' + url;
  }

}
