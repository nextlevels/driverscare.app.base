import {Component} from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {NotificationModel} from "../../models/NotificationModel";
import {StorageProvider} from "../../providers/storage/storage";
import {GlobalProvider} from "../../providers/global/global";
import {ProcessProvider} from "../../providers/process/process";
import {BookinghelperPage} from "../bookinghelper/bookinghelper";
import {Calendar} from "@ionic-native/calendar";
import * as moment from "moment";

@Component({
    selector: 'page-notifications',
    templateUrl: 'notifications.html',
})
export class NotificationsPage {

    allNotifications: NotificationModel[] = [];

    notifcation_read: NotificationModel[] = [];
    notifcation_new: NotificationModel[] = [];

    notification_status: string = 'new';

    notification_color: string[] = [];


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private api: ApiProvider,
        private global: GlobalProvider,
        private processProvider: ProcessProvider,
        private events: Events,
        private calendar: Calendar
    ) {

        this.notification_color[0] = 'green';
        this.notification_color[1] = 'green';
        this.notification_color[2] = '#2E88F4';
        this.notification_color[3] = 'red';

    }

    ionViewWillEnter() {
        this.api.getNotifications().then(data => {
            this.notifcation_read = [];
            this.notifcation_new = [];

            StorageProvider.notifications = data.response;
            StorageProvider.saveStorage("notifications", data.response);
            this.allNotifications = data.response;
            this.allNotifications.forEach(item => {
                item["color"] = this.notification_color[item.category_id];
            });

            if (StorageProvider.getStorage('readNotification')) {
                this.filterReadAndUnreadNotifications();
            } else {
                StorageProvider.saveStorage('readNotification', this.allNotifications.filter(x => x.id).map(x => x.id));
                this.notification_status = 'read';
                this.global.newNotification = 0;
                this.notifcation_read = this.getSortedByCreatedAt(this.allNotifications);
            }

            this.global.newNotification = this.notifcation_new.length;

        }, error => {
            console.error(error)
        })
    }

    markeAsRead(notification: NotificationModel) {
        let keys: number[] = StorageProvider.getStorage('readNotification');

        if (keys.find(x => x === notification.id)) {
            StorageProvider.saveStorage('readNotification', keys);
            return;
        } else {
            keys.push(notification.id);
            StorageProvider.saveStorage('readNotification', keys);
        }
        this.filterReadAndUnreadNotifications();
    }

    markeAsUnRead(notification: NotificationModel) {
        let keys: number[] = StorageProvider.getStorage('readNotification');
        let newKeys = keys.filter(x => x != notification.id);
        StorageProvider.saveStorage('readNotification', newKeys);
        this.filterReadAndUnreadNotifications();
    }

    filterReadAndUnreadNotifications() {
        this.notifcation_read = [];
        this.notifcation_new = [];

        let keys = StorageProvider.getStorage('readNotification');
        if (keys.length > 0) {
            for (let note of this.allNotifications) {
                if (keys.find(x => x == note.id)) {
                    this.notifcation_read.push(note);
                } else {
                    this.notifcation_new.push(note);
                }
            }
        } else {
            this.notifcation_new = this.allNotifications;
        }

        this.notifcation_read = this.getSortedByCreatedAt(this.notifcation_read);
        this.notifcation_new = this.getSortedByCreatedAt(this.notifcation_new);
        this.global.newNotification = this.notifcation_new.length;
    }


    makeAllAsRead() {
        StorageProvider.saveStorage('readNotification', this.allNotifications.map(x => x.id));
        this.filterReadAndUnreadNotifications();
    }

    makeAllAsUnRead() {
        StorageProvider.saveStorage('readNotification', []);
        this.filterReadAndUnreadNotifications();
    }

    handleNotification(notification: NotificationModel) {
            this.markeAsRead(notification);
        if (notification["process"] && (notification.category_id == null)) {
            this.processProvider.handle(notification["process"]);
            this.markeAsRead(notification);
        }
        else if (notification["process"] && (notification.category_id === 3)) {
            this.processProvider.handle(notification["process"]);
            this.markeAsRead(notification);
        } else if (notification.category_id === 2) {
            this.events.publish('page:change', BookinghelperPage, {'related_data': notification["related_data"]});
            this.markeAsRead(notification);
        } else if (notification.category_id === 0 || notification.category_id === 1) {
            this.global.showConfirm("Achtung", "Möchten Sie den Termin in Ihren Kalender eintragen").then(data => {

                let startDate : any = moment(notification["related_data"].time,"YYYY-MM-DD HH:mm:ss");
                let endDate : any = moment(notification["related_data"].time,"YYYY-MM-DD HH:mm:ss").add('hours',0.5);

                this.calendar.createEvent(
                    notification.title,
                    notification["related_data"].dealer_name,
                    "Termin",
                    startDate.toDate(),
                    endDate.toDate(),
                ).then(data => {
                    this.markeAsRead(notification);
                    this.global.showPrompt("Erfolgreich","Der Termin am " + startDate.format("DD.MM.YYYY") + " um " + startDate.format("HH:mm") + " wurde im Kalender eingetragen");
                },error => {
                })
            }, error => {

            })
        }
    }

    private getSortedByCreatedAt(x: any[]) : any[]{
        return x.sort((left, right) => moment(left['created_at']).diff(moment(right['created_at']))).reverse();
    }

}
