import {Component} from '@angular/core';
import {LoadingController, ModalController, NavController, NavParams, Platform} from 'ionic-angular';
import {styler, spring, listen, pointer, value, tween, physics, easing, decay} from 'popmotion';
import {Card} from "../../models/Card";
import {StorageProvider} from "../../providers/storage/storage";
import {BarcodeScanner, BarcodeScanResult} from "@ionic-native/barcode-scanner";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import set = Reflect.set;
import {Camera, CameraOptions} from "@ionic-native/camera";
import {WalletDetailPage} from "../wallet-detail/wallet-detail";
import * as $ from 'jquery';

@Component({
    selector: 'page-contact',
    templateUrl: 'wallet.html'
})

export class WalletPage {


    modalAddNewCardClosed: boolean = true;
    activeState: boolean = false;
    card: any = null;

    clickCardInfo: boolean = false;
    cardinfoSectionActive: boolean = false;

    allCard: Card[] = [];
    cardId: string = '-1';
    activeCard: Card = null;

    serviceProviders: any[] = [];

    choosenCardByAction: Card = null;
    blockUserInput: boolean = false;


    constructor(
        public navCtrl: NavController,
        private scanner: BarcodeScanner,
        private platform: Platform,
        private api: ApiProvider,
        private global: GlobalProvider,
        private camera: Camera,
        private modal: ModalController,
        private loadingController: LoadingController
    ) {

        this.allCard = StorageProvider.cards.map((card) => {
            card["is_lost"] = !!card['lost_at'];
            return card;
        });
        console.log(this.allCard);
    }

    ionViewWillEnter() {
        let timeout = 800;
        if (StorageProvider.activeCardByDealer !== -1) {
            if (this.activeState) {
                this.resetCards();
                timeout = 1000;
            }

            console.log(StorageProvider.activeCardByDealer);
            console.log(this.allCard);

            this.choosenCardByAction = this.allCard.find(x => {
                if (x["service_provider"]) {
                    return x["service_provider"].id === StorageProvider.activeCardByDealer
                }
            });
            if (this.choosenCardByAction) {
                this.blockUserInput = true;
                let card = document.getElementById(String(this.choosenCardByAction.id));

                setTimeout(() => {
                    this.setCardActive(card);
                    this.blockUserInput = false;
                }, timeout);
            }
        }
    }

    ionViewDidLoad() {

        this.api.getAllServiceProvider().then(data => {
            this.serviceProviders = data.response;
        });

        const container = document.querySelector('.card-wrapper');
        const allCards = document.getElementsByClassName("card-animation");


        for (let i = 0; i < allCards.length; i++) {
            tween({
                from: {top: (120 + (i * 8) + 'vh')},
                to: {top: (10 + (i * 8) + 'vh')},
                ease: easing.backOut,
                duration: 1500
            }).start(styler(allCards[i]).set);
        }

        listen(container, 'click').start((thisStyler) => {

            if (this.clickCardInfo) {
                this.clickCardInfo = false;
                this.modalAddNewCardClosed = false;
                this.activeState = false;
                this.cardinfoSectionActive = true;
                return;
            }
            else {
                if (this.cardinfoSectionActive) {
                    this.activeState = true;
                    this.cardinfoSectionActive = false;
                }
                thisStyler.preventDefault();


                let card = thisStyler.target.closest(".card-animation");

                if (this.activeState) {
                    this.setCardInactive(card);
                } else {
                    this.setCardActive(card);
                }
            }


        });

    }

    setCardActive(card: any) {

        const allCards = document.getElementsByClassName("card-animation");
        this.activeState = true;

        this.activeCard = card;
        this.cardId = card.id;

        console.log(card);

        tween({
            from: {top: 30 + 'vh'},
            to: {top: 2 + 'vh'},
            ease: easing.backOut,
            duration: 1500
        }).start(styler(card).set);

        card.classList.add("active-card");

        let counter = 0;
        for (let i = 0; i < allCards.length; i++) {
            if (!allCards[i].classList.contains("active-card")) {
                spring({
                    from: {top: 30 + 'vh'},
                    to: {top: (60 + counter * 8) + 'vh'},
                    ease: easing.backOut,
                    duration: 1500
                }).start(styler(allCards[i]).set);

                counter++;
            }
        }
    }

    setCardInactive(card: any) {

        StorageProvider.activeCardByDealer = -1;
        const allCards = document.getElementsByClassName("card-animation");
        this.activeState = true;

        this.activeState = false;
        this.cardId = '-1';
        this.activeCard = null;

        for (let i = 0; i < allCards.length; i++) {

            if (!allCards[i].classList.contains("active-card")) {
                tween({
                    from: {top: (60 + i * 8) + 'vh'},
                    to: {top: (10 + (i * 8) + 'vh')},
                    ease: easing.easeIn,
                    duration: 1000
                }).start(styler(allCards[i]).set);
            } else {
                tween({
                    from: {top: 2 + 'vh'},
                    to: {top: (10 + (i * 8) + 'vh')},
                    ease: easing.backOut,
                    duration: 1500
                }).start(styler(allCards[i]).set);
            }

            allCards[i].classList.remove("active-card");
        }
    }


    removeCard() {
        this.global.showConfirm('Achtung', 'Wollen Sie die Karte wirklich löschen?').then(data => {
            if (data === true) {
                if (this.cardId != '-1') {
                    this.api.removeCard(this.cardId).then(data => {
                        StorageProvider.cards = data.response;
                        StorageProvider.saveStorage("cards", data.response);
                        this.allCard = StorageProvider.cards.map((card) => {
                            card["is_lost"] = !!card['lost_at'];
                            return card;
                        });
                        this.activeState = false;

                        const allCards = document.getElementsByClassName("card-animation");

                        setTimeout(() => {

                            for (let i = 0; i < allCards.length; i++) {

                                if (!allCards[i].classList.contains("active-card")) {
                                    tween({
                                        from: {top: (60 + i * 8) + 'vh'},
                                        to: {top: (10 + (i * 8) + 'vh')},
                                        ease: easing.easeIn,
                                        duration: 1000
                                    }).start(styler(allCards[i]).set);
                                } else {
                                    tween({
                                        from: {top: 2 + 'vh'},
                                        to: {top: (10 + (i * 8) + 'vh')},
                                        ease: easing.backOut,
                                        duration: 1500
                                    }).start(styler(allCards[i]).set);
                                }


                                allCards[i].classList.remove("active-card");
                            }

                        }, 100)
                    })
                }
            }
        })
    }

    dragInit() {
        /*
              const slider = document.querySelector('.card-wrapper');
              const divStyler = styler(slider);
              const sliderY = value(0, divStyler.set('y'));

              listen(slider, 'mousedown touchstart')
                .start(() => {
                  pointer({ y: sliderY.get() })
                .pipe(({ y }) => y)
                    .start(sliderY);
                });

              listen(document, 'mouseup touchend')
                  .start(() => {
                    decay({
                      from: sliderY.get(),
                      velocity: sliderY.getVelocity(),
                      // power: 0.8,
                      // timeConstant: 350,
                      // restDelta: 0.5,
                      // modifyTarget: v => v
                    }).start(sliderY);
                  });
                  */
    }

    openCardDetails(card) {
        const modal = this.modal.create(WalletDetailPage, {
                card: card,
                providers: this.serviceProviders
            }
        );

        modal.onDidDismiss(data => {
            if(data.edit){
                this.activeState = false;
                this.resetCards();
            }
        });
        modal.present();
    }

    openActiveCardDetails() {
      console.log(this.activeCard.id);
      console.log( this.allCard.find(i => i.id == this.activeCard.id).id)
      const modal = this.modal.create(WalletDetailPage, {
          card:  this.allCard.find(i => i.id == this.activeCard.id),
          providers: this.serviceProviders
        }
      );

      modal.onDidDismiss(data => {
        if(data.edit){
          this.activeState = false;
          this.resetCards();
        }
      });
      modal.present();
    }

    openAddCardForm() {
        if (!this.serviceProviders) {
            return;
        }
        let modal = this.modal.create(WalletDetailPage, {
            card: null,
            providers: this.serviceProviders
        });
        modal.onDidDismiss(data => {

            this.resetCards();
        });
        modal.present();
    }


    getCardById() {
        if (this.cardId === "-1") return null;
        return this.allCard.find(x => String(x.id) == this.cardId);
    }

    ionViewDidLeave() {
        StorageProvider.activeCardByDealer = -1;
    }

    resetCards() {
        this.allCard = StorageProvider.cards.map((card) => {
            card["is_lost"] = !!card['lost_at'];
            return card;
        });

        const allCards = document.getElementsByClassName("card-animation");

        setTimeout(() => {

            for (let i = 0; i < allCards.length; i++) {

                if (!allCards[i].classList.contains("active-card")) {
                    tween({
                        from: {top: (60 + i * 8) + 'vh'},
                        to: {top: (10 + (i * 8) + 'vh')},
                        ease: easing.easeIn,
                        duration: 1000
                    }).start(styler(allCards[i]).set);
                } else {
                    tween({
                        from: {top: 2 + 'vh'},
                        to: {top: (10 + (i * 8) + 'vh')},
                        ease: easing.backOut,
                        duration: 1500
                    }).start(styler(allCards[i]).set);
                }

                allCards[i].classList.remove("active-card");
            }

        }, 500)
    }

    openLostCardAlert() {

        const isLost = $(this.activeCard).attr("data-lost") == true;
        const cardId = $(this.activeCard).attr("id");

        let message = 'Möchtest du die Karte als verloren melden?';
        let title = 'Karte verloren';
        let prompt = 'Die Karte wurde als verloren gemeldet.';

        let note = '';


        if (isLost) {
            message = 'Hast du die Karte wiedergefunden?';
            title = 'Karte gefunden';
            prompt = 'Die Karte wurde als gefunden gemeldet.'
        }

        this.global.showPromptInputWithCallback(title, message).then(data => {
            if (data) {
                console.log(data);
                let loading = this.loadingController.create({
                    spinner: 'circles',
                    content: 'Verloren melden...'
                });
                loading.present();
                this.api.setCardLostOrFound(this.cardId, data['note'])
                    .then(data => {
                        this.allCard.map(card => {
                            if (card.id === Number(cardId)) {
                                card["is_lost"] = true;
                                return card;
                            }
                        });
                        StorageProvider.cards = this.allCard;
                        loading.dismiss();
                        this.global.showPrompt('Erfolgreich', prompt);
                    }, err => {
                        loading.dismiss();
                    });
            }
        })
    }

}


