import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {map, take, tap} from 'rxjs/operators';

@Component({
    selector: 'page-static-content',
    templateUrl: 'static-content.html',
})
export class StaticContentPage {

    page: string = "impress";
    pageHtml: string = "";

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private api: ApiProvider
    ) {

    }

    ionViewDidLoad() {
        this.page = this.navParams.get("data");

        if (this.page) {
            this.api.getStaticPage(this.page)
                .pipe(
                    map(res => {
                        return res[0].data;
                    }))
                .subscribe(data => {
                    this.pageHtml = data;
                },err => {
                    console.log(err);
                })
        }

    }

    close() {
        this.navCtrl.pop();
    }


}
