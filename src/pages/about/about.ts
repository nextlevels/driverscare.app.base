import {Component, ViewChild} from '@angular/core';
import {ModalController, NavController, Slides} from 'ionic-angular';
import {Vehicle} from "../../models/Vehicle";
import {StorageProvider} from "../../providers/storage/storage";
import * as moment from "moment";
import {InfoModalPage} from "../info-modal/info-modal";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {TabsPage} from "../tabs/tabs";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

   vehicle : Vehicle;
   kfzinfo: any;
   mileAge : number = 0;

   poolCars : Vehicle[] = [];

  @ViewChild(Slides) slides: Slides;

  slideOpts: any = {
    slidesPerView: 'auto',
    loop: true,
    speed: 500,
    zoom: true,
    centeredSlides: false,
    freeModeSticky: false,

  };

  constructor(public navCtrl: NavController,
              public modalController: ModalController,
              private api: ApiProvider,
              private global: GlobalProvider,
  ) {


    this.vehicle = StorageProvider.vehicle;
    this.kfzinfo = 'Fahrzeugschein';
    this.mileAge = this.vehicle.mileage;

    this.api.getPoolCars()
      .then(data => {
        this.poolCars = data.response;
      }, error => {
       });

  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
  }

  setCar(car){
    this.api.setPoolCar(car.id)
      .then(data => {
        StorageProvider.vehicle = data.response;
        console.log(StorageProvider.vehicle.company.app_color);
        if (StorageProvider.vehicle.company.app_color) {
          let color = StorageProvider.vehicle.company.app_color;
          GlobalProvider.updateColor(color);
        }
        StorageProvider.saveStorage("vehicle", data.response);

        this.api.getCategories()
          .then(data => {
            StorageProvider.processCategories = data.response;
            StorageProvider.saveStorage("processCategories", data.response);
          }, error => {
            this.global.showAlert(error['errorMessage'],true);
          })
          .then(() => {
            this.api.getFavorites()
              .then(data => {
                if (data.success) {
                  StorageProvider.favourites_sj.next({
                    toggle: false,
                    data: data.response
                  });
                  StorageProvider.saveStorage("favorites", data.response);
                }
              }, error => {
                this.global.showAlert(error, true);
              })
              .then(() => {

              })
          });

      }, error => {
      });

  }


  getDate(date : any){
      if(!date)
          return '';
      return moment(date, "YYYY.MM.DD HH:mm:ss").format('MM/YYYY');
  }

  carActive(car){
   return  StorageProvider.vehicle.license_plate == car.license_plate;
  }


   presentModal(info : string,car) {
     this.modalController.create(InfoModalPage, {kfzinfo: info, car: car }).present()
  }
}
