import {Component, ElementRef, NgZone, OnInit, Renderer2, ViewChild} from '@angular/core';
import {NavController, NavParams, Platform} from 'ionic-angular';
import {StorageProvider} from "../../providers/storage/storage";

import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {Dealer} from "../../models/Dealer";
import {DealerinfoPage} from "../dealerinfo/dealerinfo";
import {_GOOGLE_MAPS_KEY} from "../../config";
import {Geolocation} from "@ionic-native/geolocation";
import {styler, spring, listen, pointer, value, tween, physics, easing, decay} from 'popmotion';
import {ServiceProvider} from "../../models/ServiceProvider";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'page-maplist',
    templateUrl: 'maplist.html'
})
export class MaplistPage implements OnInit {

    dealers: Dealer[];
    googleDealers: any[] = [];
    filter: any;
    searchField: string;

    watchPositionObservable: any;

    service_id: any = -1;
    service_provider: ServiceProvider;
    showSpinner: boolean = false;
    showHelp: boolean = false;

    currentPositionMarker: any = null;

    firstVisit: boolean = false;
    itemDetail: any = null;

    googleMaps: any;
    map: any;
    markers: any[] = [];
    showMarkers: any[] = [];

    firstInitSearch : boolean = true;

    currentPosition: any = {};


    maxZoom: number = 6;
    radius: number = 500;

    @ViewChild('map') mapElementRef: ElementRef;

    constructor(public platform: Platform,
                public navCtrl: NavController,
                private callNumber: CallNumber,
                private api: ApiProvider,
                private iab: InAppBrowser,
                private global: GlobalProvider,
                public navParams: NavParams,
                private renderer: Renderer2,
                private geo: Geolocation,
                private ngZone: NgZone
    ) {


    }

    ionViewWillEnter() {
        this.navCtrl.swipeBackEnabled = false;
        if(StorageProvider.processPolicy != ''){
          this.showHelp = true;
        }
    }

    ionViewWillLeave() {
        this.navCtrl.swipeBackEnabled = true;
    }

    ionViewDidLeave() {
        this.navCtrl.swipeBackEnabled = true;
        if(this.watchPositionObservable){
            this.watchPositionObservable.unsubscribe();
        }
    }

    ionViewDidLoad() {

        this.initDrag();

        this.dealers = [];
        this.showSpinner = true;


        this.loadMap().then((googleMaps) => {
            this.googleMaps = googleMaps;
            const mapEl = this.mapElementRef.nativeElement;

            this.map = new googleMaps.Map(mapEl, {
                center: {lat: 52.520008, lng: 13.404954},
                zoom: 16,
                disableDefaultUI: true,
                styles: GlobalProvider.mapStyle
            });

            googleMaps.event.addListenerOnce(this.map, 'idle', () => {
                this.renderer.addClass(mapEl, 'visible');
                this.locateMap(googleMaps);
            })

        }).catch(err => {
            console.error(err);
        })
    }

    async ngOnInit() {

        await this.platform.ready();
    }


    loadMap(): Promise<any> {
        const win = window as any;
        const googleModule = win.google;
        if (googleModule && googleModule.maps) {
            return Promise.resolve(googleModule.maps)
        }
        return new Promise((resolve, reject) => {
            const script = document.createElement('script');
            script.src = 'https://maps.googleapis.com/maps/api/js?key=' + _GOOGLE_MAPS_KEY;
            script.async = true;
            script.defer = true;
            document.body.appendChild(script);
            script.onload = () => {
                const loadedGoogleModule = win.google;
                if (loadedGoogleModule && loadedGoogleModule.maps) {
                    resolve(loadedGoogleModule.maps);
                } else {
                    reject('Google Maps SDK not available');
                }
            }
        })
    }

    chooseGetPlace(movedByUser : boolean = false) {
        if (this.map.getZoom() > this.maxZoom) {

            const coords = {lat: this.map.center.lat(), lng: this.map.center.lng()};

            if (this.filter) {
                this.getPlaces(this.googleMaps, null, coords);
            } else {
                this.radius = 150 * Math.pow(2,(17 - this.map.getZoom()));
                this.getPlacesByOwnDealer(coords);
            }
        }
    }

    locateMap(googleMaps: any) {
        let myBounds = new googleMaps.LatLngBounds();
        this.geo.getCurrentPosition().then(data => {

            const coords = {lat: data.coords.latitude, lng: data.coords.longitude};
            this.currentPosition = coords;
            this.map.panTo(this.currentPosition);

            this.googleMaps.event.addListener(this.map, 'dragend', () => {
                this.firstInitSearch = false;
                this.chooseGetPlace();
            });
            this.googleMaps.event.addListener(this.map, 'zoom_changed', () => {
                this.chooseGetPlace();
            });

            this.currentPositionMarker = new googleMaps.Marker({
                position: coords,
                map: this.map,
                icon: {url: './assets/imgs/bluecircle.png'}
            });

            this.googleMaps.event.addListener(this.currentPositionMarker, 'click', (ev) => {
                this.moveToLocation(ev.latLng);
            });

            this.watchPositionObservable = this.geo.watchPosition().subscribe(() => {

                this.geo.getCurrentPosition().then(abc => {

                    const coords = {lat: abc.coords.latitude, lng: abc.coords.longitude};

                    if (this.currentPositionMarker)
                        this.currentPositionMarker.setMap(null);

                    this.currentPositionMarker = new googleMaps.Marker({
                        position: coords,
                        map: this.map,
                        icon: {url: './assets/imgs/bluecircle.png'}
                    });
                });

            });


            this.service_id = this.navParams.get('param').service_id;
            this.service_provider = this.navParams.get('param').service_provider;



                this.api.getServiceProviderForServiceGeo(this.service_id,StorageProvider.activeProcess.id,coords.lat,coords.lng)
                    .then(data => {
                     console.log(data);
                        this.showSpinner = false;

                        this.service_provider = data.response;

                        if (this.service_provider.google_maps_active) {
                            this.filter = this.service_provider;
                            this.getPlaces(googleMaps, myBounds, coords);
                        } else {
                          console.log('test_1');
                            this.getPlacesByOwnDealer(coords, true);
                        }

                    }, error => {
                        this.global.showAlert(error.errorMessage, true);
                    })



        });
    }


    rad(x) {
        return x * Math.PI / 180;
    };

    getDistance(p1, p2) {
        let R = 6378137;
        let dLat = this.rad(p2.lat - p1.lat);
        let dLong = this.rad(p2.lng - p1.lng);
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) * Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    };

    changeDealerListByDistance() {
        this.dealers.sort((a, b) => {
            return a.distance - b.distance;
        })
    }

    getPlacesByOwnDealer(coords: any, fitBounds: boolean = false) {

        let params = {
            lat: coords.lat,
            long: coords.lng,
            radius: this.radius,
            service_id: this.service_id,
            process_id:  StorageProvider.activeProcess.id
        };

        this.api.getDealersForService(params)
            .then(data => {

                if (data.response && data.response.length === 0) {
                  this.setMapOnAll(null);
                  this.dealers = [];
                  if(this.map.getZoom() > this.maxZoom && this.firstInitSearch)
                    this.map.setZoom(this.map.getZoom()-1);
                  return;
                }


                StorageProvider.services[this.service_id] = data.response;
                StorageProvider.dealers = data.response;
                this.dealers = StorageProvider.dealers;
                this.setMapOnAll(null);
                this.markers = [];
                let myBounds = new this.googleMaps.LatLngBounds();
                myBounds.extend(new this.googleMaps.LatLng(params.lat, params.long));


                this.ngZone.run(() => {
                    this.dealers.filter(x => {
                        return x.distance = this.getDistance(
                            {
                                lat: this.currentPosition.lat,
                                lng: this.currentPosition.lng,
                            }, {
                                lat: x.lat,
                                lng: x.long,
                            });
                    });
                    this.changeDealerListByDistance();
                    this.dealers.forEach((item: any) => {

                        let marker = new this.googleMaps.Marker({
                            position: {lat: Number(item["lat"]), lng: Number(item["long"])},
                            map: this.map,
                            title: '...',
                            snippet: '...',
                            icon: {url: './assets/imgs/maps-and-flags.png'},
                            dealer: item
                        });
                        this.googleMaps.event.addListener(marker, 'click', (ev) => {
                            if (marker.dealer) {
                                this.moveToLocation(ev.latLng);
                                this.itemDetail = marker.dealer;
                            }
                        });
                        this.markers.push(marker);


                        if (myBounds)
                            myBounds.extend(new this.googleMaps.LatLng(item.lat, item.long));

                    });
                });

                if (fitBounds) {
                    if (myBounds) {
                        this.map.fitBounds(myBounds);
                        if (this.map.zoom > 16) {
                            this.map.setZoom(16)
                        }
                    }
                }

            }, error => {
                this.global.showAlert(error.errorMessage, true);
            })


    }

    moveToLocation(latLng) {
        this.map.panTo(latLng);
        setTimeout(() => {
            this.map.setZoom(16);
        }, 300)
    }

    closeDetail() {
        this.itemDetail = null;
        this.map.setZoom(11);
    }

    getRadiusOfMap(): string {
        let bounds = this.map.getBounds();
        let center = bounds.getCenter();
        let ne = bounds.getNorthEast();
        let r = 3963.0;
        let lat1 = center.lat() / 57.2958;
        let lon1 = center.lng() / 57.2958;
        let lat2 = ne.lat() / 57.2958;
        let lon2 = ne.lng() / 57.2958;
        let dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));
        return String(dis * 1000);
    }

    getPlaces(googleMaps: any, myBounds: any, coords: any, zoom: number = this.map.getZoom()) {

        let bounds = this.map.getBounds();

        let center = bounds.getCenter();
        let ne = bounds.getNorthEast();
        let r = 3963.0;
        let lat1 = center.lat() / 57.2958;
        let lon1 = center.lng() / 57.2958;
        let lat2 = ne.lat() / 57.2958;
        let lon2 = ne.lng() / 57.2958;

        let dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
            Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));

        dis *= 1000;


        if (this.filter) {
            let radius = String(dis);
          this.api.getNearbyPlaces(coords.lat, coords.lng, radius, '', this.filter.google_maps_key).then(data => {
            console.log(data);
                if (data && data.results.length === 0) {
                    this.setMapOnAll(null);
                    this.dealers = [];
                    if(zoom > this.maxZoom && this.firstInitSearch)
                        this.map.setZoom(zoom-1);
                    return;
                }
                this.dealers = [];
                this.setMapOnAll(null);
                this.markers = [];
                data.results.forEach((item: any) => {

                    let marker = new this.googleMaps.Marker({
                        position: {lat: item.geometry.location.lat, lng: item.geometry.location.lng},
                        map: this.map,
                        title: [data.name].join("\n"),
                        snippet: data.street + " " + data.house_number + " \n" + data.postal_code + " " + data.city + " \n" + data.telephone,
                        icon: {url: './assets/imgs/maps-and-flags.png'}
                    });
                    this.markers.push(marker);

                    if (myBounds)
                        myBounds.extend(new googleMaps.LatLng(item.geometry.location.lat, item.geometry.location.lng));

                  let pictureObject:any;
                  if(this.service_provider.file_banner){
                     pictureObject = this.service_provider.file_banner;
                  }else {
                    let picture = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=' + (item.photos ? item.photos[0].photo_reference : '') + '&key=AIzaSyDPe7zfvgy667ScpsTzZtHbQ1MGvxtFzlI';
                    pictureObject = new Object();
                    pictureObject['path'] = picture;
                  }


                    this.ngZone.run(() => {

                        let distance = this.getDistance(
                            {
                                lat: this.currentPosition.lat,
                                lng: this.currentPosition.lng,
                            }, {
                                lat: item.geometry.location.lat,
                                lng: item.geometry.location.lng,
                            });
                        let t_dealer: any = {
                            distance: distance,
                            lat: item.geometry.location.lat,
                            long: item.geometry.location.lng,
                            name: item.name,
                            street: item.vicinity,
                            file_banner: pictureObject,
                            places_id: item.place_id
                        };

                        this.dealers.push(t_dealer as Dealer);
                        this.googleDealers.push(data);

                        this.googleMaps.event.addListener(marker, 'click', (ev) => {
                            if (marker) {
                                this.moveToLocation(ev.latLng);
                                this.itemDetail = t_dealer;
                            }
                        });
                    });
                });

                if (data.results.length > 0) {
                    if (myBounds) {
                        this.map.fitBounds(myBounds);
                        if (this.map.zoom > 16) {
                            this.map.setZoom(16)
                        }
                    }
                }else{
                    // this.map.setZoom(10)
                }

            }, err => {

            });
        }

    }

    setMapOnAll(map) {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }

    refreshDealers() {
        this.dealers.forEach((data: Dealer) => {
            this.markers.push({
                position: {"lat": data.lat, 'lng': data.long},
                title: [data.name].join("\n"),
                snippet: data.street + " " + data.house_number + " \n" + data.postal_code + " " + data.city + " \n" + data.telephone,
                icon: {url: './assets/imgs/maps-and-flags.png'}
            })

        });
    }

    searchDealers() {
        let coords: any;
        let myBounds = new this.googleMaps.LatLngBounds();

        if (this.searchField) {
            this.api.getAdressCoords('DE, '+ this.searchField).then(data => {
                if (data["results"].length > 0 && data["status"] === "OK") {
                    coords = data["results"][0]["geometry"]["location"];
                  this.map.setZoom(12);
                    this.map.panTo(coords);
                    if (this.filter){
                      this.getPlaces(this.googleMaps, myBounds, coords);
                    }
                    else{
                      this.getPlacesByOwnDealer(coords, true);
                    }
                }
            })
        }
    }

    action(dealer: Dealer) {
        if (this.filter) {
            this.api.getNearbyPlacesDetails(dealer.places_id).then(data => {
                if (data) {
                    if (data.result) {
                        dealer.telephone = data.result.formatted_phone_number;
                        dealer.website = data.result.website;
                    }

                    this.navCtrl.push(DealerinfoPage, {
                        dealer: dealer,
                        service_provider: this.service_provider
                    })
                }
            }, error => {
            })
        } else {
            this.navCtrl.push(DealerinfoPage, {dealer: dealer})
        }
    }

    actionCall(number: string) {
        this.callNumber.callNumber(number, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    actionWeb(website: string) {
        const browser = this.iab.create(website, '_blank');
    }

    initDrag() {
        const balltrigger = document.querySelector('#container-drag-element-trigger');
        const ball = document.querySelector('#container-drag-element');
        const map = styler(document.querySelector('#map_canvas'));
        const divStyler = styler(ball);
        const ballY = value(0, (e) => {
                let listHeight = 100 - ((e.y / document.body.clientHeight) * 100);
                let mapHeight = ((e.y / document.body.clientHeight) * 100);
                divStyler.set({height: listHeight + '%'});
                map.set({height: mapHeight + '%'});
            }
        );

        listen(balltrigger, 'mousedown touchstart')
            .start((e) => {
                e.preventDefault();
                pointer().start(ballY);
            });

        listen(document, 'mouseup touchend')
            .start((e) => {
                ballY.stop();
                let currentElementHeight = this.getNumberHeight(divStyler.get('height'));

                if (currentElementHeight < 40) {
                    divStyler.set({height: 15 + '%'});
                    map.set({height: 85 + '%'});
                }
            });
    }

    getNumberHeight(height: any): number {
        return height.substring(0, height.length - 1) * 1;
    }


    onInput(ev: any) {
        console.log(event);
    }

    onCancel(ev: any) {
        this.searchField = '';
    }

    resetDealerAndMarkers() {
        this.setMapOnAll(null);
        this.dealers = [];
        this.markers = [];
    }

}
