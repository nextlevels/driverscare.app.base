import {Component} from '@angular/core';
import {NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {Card} from "../../models/Card";
import {BarcodeScanner, BarcodeScanResult} from "@ionic-native/barcode-scanner";
import {StorageProvider} from "../../providers/storage/storage";
import {easing, styler, tween} from "popmotion";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {Camera, CameraOptions} from "@ionic-native/camera";


@Component({
    selector: 'page-wallet-detail',
    templateUrl: 'wallet-detail.html',
})
export class WalletDetailPage {

    card: Card = null;
    providers: any[] = [];
    selectedProviderId: number = 0;
    showSpinner: boolean = false;

    cardColor: string = "#3498DB";
    cardname: string = "";
    scannumber: string = "";

    scanResult: BarcodeScanResult = <BarcodeScanResult>{
        format: "QR_CODE",
        text: "",
        cancelled: false
    };

    images: any[] = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private scanner: BarcodeScanner,
        private platform: Platform,
        private api: ApiProvider,
        private global: GlobalProvider,
        private camera: Camera,
        private viewCtrl : ViewController
    ) {
    }

    ionViewDidLoad() {
        let card = this.navParams.get('card');
        let providers = this.navParams.get('providers');
        if (card) {
            this.card = card;
            this.cardname = card.name;
            this.cardColor = card.card_color;
            this.scannumber = card.code_number;

            if (card.service_provider) {
                this.selectedProviderId = card.service_provider.id;
            }

            if (card.front_image) {
                this.images[0] = card.front_image.path;
            }
            if (card.back_image) {
                this.images[1] = card.back_image.path;
            }
            if(card.code_type){
                this.scanResult.format = card.code_type
            }



        }
        if (providers) {
            this.providers = providers;
        }

    }

    closeModal() {
        this.viewCtrl.dismiss({ edit: false });
    }


    addNewCard() {
        if (this.platform.is("cordova")) {
            this.scanner.scan({
                preferFrontCamera: false, // iOS and Android
                showFlipCameraButton: true, // iOS and Android
                showTorchButton: true, // iOS and Android
                torchOn: true, // Android, launch with the torch switched on (if available)
                orientation: "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
                disableAnimations: true, // iOS
                disableSuccessBeep: false // iOS and Android
            }).then(data => {
                this.scanResult = data;
                this.scannumber = this.scanResult.text
            })
        }
    }


    cardSave() {

        let params = {
            'parent_id': 0,
            'card_color': this.cardColor,
            'code_number': this.scannumber,
            'code_type': this.scanResult.format,
            'name': this.cardname,
            'service_provider_id': this.selectedProviderId
        };


        this.showSpinner = true;


        if (this.card) { //EDIT
          params["card_id"] = this.card.id;

            if (this.images[0] && !this.images[0].includes("https://")) {
                params['front_image'] = this.images[0];
            }
            if (this.images[1] && !this.images[0].includes("https://")) {
                params['back_image'] = this.images[1];
            }

            this.api.updateCard(params).then(data => {
                StorageProvider.cards = data.response;
                StorageProvider.saveStorage("cards", data.response);
                this.viewCtrl.dismiss({ edit: true });
            }, err => {
                this.global.showAlert(err.errorMessage, true);
            }).then(() => {
                this.showSpinner = false;
            })
        } else { //NEW
            if (this.images[0]) {
                params['front_image'] = this.images[0];
            }
            if (this.images[1]) {
                params['back_image'] = this.images[1];
            }
            this.api.addCard(params).then(data => {
                StorageProvider.cards = data.response;
                StorageProvider.saveStorage("cards", data.response);
                this.viewCtrl.dismiss({ edit: false });
            }, err => {
                this.global.showAlert(err.errorMessage, true);
            }).then(() => {
                this.showSpinner = false;
            })
        }


    }

    getImage(front: boolean = true) {
        const options: CameraOptions = {
            quality: 30,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true
        };

        this.camera.getPicture(options).then((imageData) => {

            if (front) {
                this.images[0] = this.mod(imageData);
            } else {
                this.images[1] = this.mod(imageData);
            }

        }, (err) => {
            console.log(err);
        });
    }

    mod(url: string) {
        return 'data:image/png;base64,' + url;
    }


}
