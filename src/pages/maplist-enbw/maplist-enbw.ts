import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Content, ModalController, NavController, NavParams, Platform} from 'ionic-angular';
import {StorageProvider} from "../../providers/storage/storage";

import {CallNumber} from "@ionic-native/call-number";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import {Dealer} from "../../models/Dealer";
import {DealerinfoPage} from "../dealerinfo/dealerinfo";
import {_GOOGLE_MAPS_KEY} from "../../config";
import {Geolocation} from "@ionic-native/geolocation";
import {ChargeStationsPage} from "../charge-stations/charge-stations";
import {styler, spring, listen, pointer, value,tween,physics,easing,decay} from 'popmotion';

export interface i_chargeDetail {
    head: any
    address: any
    chargePoints: i_chargePoints
    maxPower: any,
    types: any
    auth: i_chargeAuth[]
    operator: any
    openingHours: any
    tariffs: i_chargeTariffs
    tariffInformation: i_chargeTariffsInformations
    additionalInfo: i_chargeAdditionalInformations
    resctictionInfos: any,
    original_station: any;
}

interface i_chargePoints {
    available: any,
    max: any
}

interface i_chargeAuth {
    mode: any;
    img: any;
    title: any;
}

interface i_chargeTariffs {
    name: any;
    img: any;
    tariffDescription: any;
}

interface i_chargeTariffsInformations {
    head: any
    img: any;
    content: any[];
}

interface i_chargeAdditionalInformations {
    head: any;
    content: any;
}

@Component({
  selector: 'page-maplist-enbw',
  templateUrl: 'maplist-enbw.html',
})
export class MaplistEnbwPage implements OnInit {

    chargeDetail: i_chargeDetail;

    dealers: Dealer[];
    filter: any;
    searchField: string;

    googleMaps: any;
    map: any;

    stations: any[] = [];
    markers: any[] = [];
    fromLat: any = 0;
    toLat: any = 0;
    fromLon: any = 0;
    toLon: any = 0;

    myLocationMarker : any;
    filterModalClosed : boolean = true;

    apimSubscriptionKey: any = null;
    imagePath: any = "https://ev-web.azurewebsites.net/assets/";
    language: any = 'de';
    translate: any = {};
    showEnBWInfo: any = true;
    mapBoxOffset: any = 0.5;
    overlay: any = {};
    divName: any = 'map';

    colors: any = {
        green: '#94c11c',
        white: '#fff',
        red: '#E2001A',
        grau: '#888888',
        grau70: "#686868",
        black: "#000",
        silver: "#979797",
        horizonOrange: '#ff9900',
        blue: '#000099'
    };

    serviceUrl: any = null;
    headers: any = null;

    stationStatus: any = {
        available: 'AVAILABLE',
        unknown: 'UKNOWN',
        unavailable: 'UNAVAilAble'
    };

    plugStatus: any = {
        reserved: 'RESERVED',
        available: 'AVAILABLE',
        occupied: 'OCCUPIED',
        outOfService: 'OUT_OF_SERVICE',
        unknown: 'UNKNOWN'
    };

    authenticationModelsStatus: any = {
        remote: 'REMOTE',
        card: 'MOBILITY_PLUS_CHARGE_CARD',
        others: 'OTHERS'
    };

    coordinatesStatus: any = {
        new: 'NEW', // On Map Created
        smaller: 'SMALLER', // On Scroll But inside the map coords. * 2
        bigger: 'BIGGER' // On Scroll But outside the map coords. * 2
    };




    @ViewChild('map') mapElementRef: ElementRef;

    constructor(public platform: Platform,
                public navCtrl: NavController,
                private callNumber: CallNumber,
                private api: ApiProvider,
                private iab: InAppBrowser,
                private global: GlobalProvider,
                public navParams: NavParams,
                private renderer: Renderer2,
                private geo: Geolocation,
                private modal: ModalController,
    ) {}

    ionViewWillEnter() {
        this.navCtrl.swipeBackEnabled = false;
    }

    ionViewDidLeave() {
        this.navCtrl.swipeBackEnabled = true;
    }

    ionViewDidLoad() {
        this.initDrag();

        this.dealers = StorageProvider.dealers;
        this.filter = this.navParams.get('param');

        if (this.filter["type"] === "enbw") {
        }

        this.initMap({
            div: 'map',
            colors: {
                green: '#94c11c',
                white: '#fff',
                red: '#E2001A',
                grau: '#888888',
                grau70: "#686868",
                black: "#000",
                silver: "#979797",
                horizonOrange: '#ff9900',
                blue: '#000099'
            },
            language: 'de',
            showEnBWInfo: true,
            serviceUrl: 'https://api.enbw.com/emobility-public/api/v1/chargestations',
            apimSubscriptionKey: '6482d162588a4746a278a29a08aa906e',
            imagePath: '/assets/',
            mapBoxOffset: 0.5 // set the offset of the larger map box, value > 0
        })

    }

    async ngOnInit() {
        await this.platform.ready();
    }

    initMap(config: any) {

        if (config == null || config.div != this.divName) {
            return;
        }

        this.language = !this.isNullOrEmpty(config.language) ? config.language : this.language;
        this.translate = this.language === 'de' ? this.de : this.en;
        this.showEnBWInfo = config.showEnBWInfo != null ? config.showEnBWInfo : this.showEnBWInfo;
        this.serviceUrl = !this.isNullOrEmpty(config.serviceUrl) ? config.serviceUrl : this.serviceUrl;
        this.imagePath = !this.isNullOrEmpty(config.imagePath) ? config.imagePath : this.imagePath;
        this.apimSubscriptionKey = !this.isNullOrEmpty(config.apimSubscriptionKey) ? config.apimSubscriptionKey : this.apimSubscriptionKey;
        this.mapBoxOffset = config.mapBoxOffset != null ? config.mapBoxOffset : this.mapBoxOffset;

        this.getColors(config.colors);

        this.loadMap().then((googleMaps) => {
            this.googleMaps = googleMaps;
            const mapEl = this.mapElementRef.nativeElement;

            this.map = new googleMaps.Map(mapEl, {
                zoom: 10,
                mapTypeId: googleMaps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                center: {
                    lat: 48.8790934,
                    lng: 8.7637405
                }
            });

            this.googleMaps.event.addListenerOnce(this.map, 'idle', () => {
                this.renderer.addClass(mapEl, 'visible');
            });

            if (this.map != null) {
                this.setCurrentLocation();
            }


        }).catch(err => {
            console.error(err);
        })

    }


    loadMap(): Promise<any> {
        const win = window as any;
        const googleModule = win.google;
        if (googleModule && googleModule.maps) {
            return Promise.resolve(googleModule.maps)
        }
        return new Promise((resolve, reject) => {
            const script = document.createElement('script');
            script.src = 'https://maps.googleapis.com/maps/api/js?key=' + _GOOGLE_MAPS_KEY;
            script.async = true;
            script.defer = true;
            document.body.appendChild(script);
            script.onload = () => {
                const loadedGoogleModule = win.google;
                if (loadedGoogleModule && loadedGoogleModule.maps) {
                    resolve(loadedGoogleModule.maps);
                } else {
                    reject('Google Maps SDK not available');
                }
            }
        })
    }

    locatePosition(){
        this.geo.getCurrentPosition().then(data => {
            const coords = {lat: data.coords.latitude, lng: data.coords.longitude};
            this.setOwnLocationMarker(coords);
        });
    }

    /*********
     *
     *
     */

    setOwnLocationMarker(coords : any){
        this.map.setZoom(17);
        this.map.setCenter(coords);

        if(!this.isNullOrEmpty(this.myLocationMarker))
            return;

        this.myLocationMarker = new this.googleMaps.Marker({
            clickable: false,
            icon: new this.googleMaps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                new this.googleMaps.Size(22,22),
                new this.googleMaps.Point(0,18),
                new this.googleMaps.Point(11,11)),
            shadow: null,
            zIndex: 999,
            map: this.map,
            position: coords
        });
    }

    closeStationDetailsDiv() {
        let detailsDiv = this.createOrGetDetailsDiv();
        if (detailsDiv != null) {
            detailsDiv.style.visibility = 'hidden';
            detailsDiv.innerHTML = '';
        }
    }

    createOrGetDetailsDiv() {
        let detailsDiv = document.getElementById('station-details');
        if (detailsDiv != null) {
            detailsDiv.style.visibility = 'visible';
            return detailsDiv;
        }

        // does not exist
        detailsDiv = document.createElement('div');
        detailsDiv.id = 'station-details';
        let mapDiv = document.getElementById('map');
        if (mapDiv != null) {
            mapDiv.parentNode.insertBefore(detailsDiv, document.getElementById('map'));
            detailsDiv.style.visibility = 'visible';
        } else {
            this.globalErrorHandler(this.translate.i18n_FailedCreatingDetailsDiv);
        }

        return detailsDiv;
    }

    mapEvent(dragend) {
        if (this.map != null && this.map.getBounds() != null) {
            let initBounds = this.map.getBounds();
            if (dragend == true) {
                let newFromLat = initBounds.getSouthWest() != null ? initBounds.getSouthWest().lat() : 0;
                let newFromLon = initBounds.getSouthWest() != null ? initBounds.getSouthWest().lng() : 0;
                let newToLat = initBounds.getNorthEast() != null ? initBounds.getNorthEast().lat() : 0;
                let newToLon = initBounds.getNorthEast() != null ? initBounds.getNorthEast().lng() : 0;
                if (this.checkMapCoordsStatus(newFromLat, newFromLon, newToLat, newToLon) === this.coordinatesStatus.smaller) {
                    // Don't recall the Server
                    return;
                }
            }

            let extendBounds = this.getExtendedBounds(initBounds);

            this.fromLat = extendBounds.getSouthWest() != null ? extendBounds.getSouthWest().lat() : 0;
            this.fromLon = extendBounds.getSouthWest() != null ? extendBounds.getSouthWest().lng() : 0;
            this.toLat = extendBounds.getNorthEast() != null ? extendBounds.getNorthEast().lat() : 0;
            this.toLon = extendBounds.getNorthEast() != null ? extendBounds.getNorthEast().lng() : 0;

            this.getStations();
        }
    }

    searchDealers(){
        let coords : any;
        this.api.getAdressCoords(this.searchField).then(data => {
            if(data["results"].length > 0){
                coords = data["results"][0]["geometry"]["location"];
                this.stations = [];
                let center = new this.googleMaps.LatLng(coords.lat, coords.lng);
                this.map.panTo(center);
                setTimeout(() => {
                    this.map.setZoom(13);
                }, 300)
            }
        })
    }


    toggleChargeStationsOnlyWithMobilityPlus : boolean = true;
    toggleChargeStationsAlwaysOpen : boolean = false;
    toggleChargeStationsOnlyAvailable : boolean = false;
    rangeMaxKw : number = 0;


    authenticationMethodsFilter : string = '';
    originalStations : any[] = [];

    filterRange(stations){
        if(this.rangeMaxKw === 0){
            return stations;
        }
        return stations.filter(x => this.rangeMaxKw <= x.maxPowerInKw);
    }

    changeChargeStationsMobilityPlus(){
        this.getStations();
    }

    handleStationsForMarkersAndList(stations){
        if (stations != null && stations.length !== 0) {
            this.addStationsMarkerToMap(stations);

            if (this.map.zoom < 11) {
                this.stations = [];
                this.listMoveBottom();
            } else {
                if(this.stations.length == 0){
                    this.listMoveTop();
                }
                this.addStationsToList(stations);
            }
        }
    }

    filterChargeStations(stations){
        let filterStations = stations;
        switch (true){
            case this.toggleChargeStationsOnlyAvailable === true:
                filterStations = filterStations.filter(x => x.availableChargePoints > 0);
            case this.toggleChargeStationsAlwaysOpen === true:
                filterStations = filterStations.filter(x => x.alwaysOpen === true);
        }
        return filterStations;
    }

    getStations() {

        this.authenticationMethodsFilter = this.toggleChargeStationsOnlyWithMobilityPlus ? 'MOBILITY_PLUS_CHARGE_CARD' : '';

        this.api.getChargeStations(
            String(this.fromLat),
            String(this.toLat),
            String(this.fromLon),
            String(this.toLon),
            this.authenticationMethodsFilter
        ).then(stations => {
            this.originalStations = stations;

            let stationsFilter = this.filterChargeStations(stations);
            stationsFilter = this.filterRange(stationsFilter);

            this.handleStationsForMarkersAndList(stationsFilter);
        }, err => {
            console.log(err);
            // this.globalErrorHandler(this.translate.i18n_ServerFailed);
        })

    }

    addStationsToList(stations) {
        this.stations = [];
        this.stations = stations;
        this.changeStationListByDistance();
    }

    changeStationListByDistance() {
        this.stations.forEach(item => {
            item.distance = this.getDistance(this.map.getCenter(), {
                lat: item.lat,
                lng: item.lon
            });
        });
        this.stations.sort((a, b) => {
            return a.distance - b.distance;
        })
    }

    showStationDetails(station) {

        this.moveToLocation(station);

        this.api.getChargeDetail(station.stationId).then(station => {
            this.chargeDetail = this.createStationDetails(station);
            // this.content.scrollTo(0,0,0).then();
        }, err => {
        });

    }

    moveToLocation(station) {
        let center = new this.googleMaps.LatLng(station.lat, station.lon);
        this.map.panTo(center);
        setTimeout(() => {
            this.map.setZoom(16);
        }, 300)
    }


    addStationsMarkerToMap(stations) {
        this.removeAllMarkers();
        if (!this.isEmptyArray(stations)) {
            for (let i = 0; i < stations.length; i++) {
                let chargePoints = stations[i].numberOfChargePoints != null ? stations[i].numberOfChargePoints : 0;
                let grouped = stations[i].grouped != null ? stations[i].grouped : false;
                let lat = stations[i].lat != null ? stations[i].lat : 0;
                let lng = stations[i].lon != null ? stations[i].lon : 0;
                let availableChargePoints = stations[i].availableChargePoints != null ? stations[i].availableChargePoints : 0;
                let numberOfChargePoints = stations[i].numberOfChargePoints != null ? stations[i].numberOfChargePoints : 0;
                let stationId = stations[i].stationId != null ? stations[i].stationId : '';
                let unknownStateChargePoints = stations[i].unknownStateChargePoints != null ? stations[i].unknownStateChargePoints : 0;

                // calculate the availability Percentage between 0 to 1
                let avaiablePercentage = 0;
                if (availableChargePoints !== 0 || numberOfChargePoints !== 0) {
                    avaiablePercentage = (availableChargePoints * 100 / numberOfChargePoints) / 100;
                }

                // Check the status of the Station
                let status = this.stationStatus.available;
                if (availableChargePoints == 0 && unknownStateChargePoints === 0) {
                    status = this.stationStatus.unavailable;
                } else if (availableChargePoints == 0 && unknownStateChargePoints === numberOfChargePoints) {
                    status = this.stationStatus.unknown;
                }

                // Draw Marker Icon
                let marker = new this.googleMaps.Marker({
                    position: {lat: lat, lng: lng},
                    map: this.map,
                    draggable: false,
                    icon: this.createMarker(grouped, chargePoints, avaiablePercentage, status),
                    stationId: stationId,
                    viewPort: stations[i].viewPort
                });


                if (!grouped && !this.isNullOrEmpty(stationId)) {
                    this.googleMaps.event.addListener(marker, 'click', () => {

                        if (!this.isNullOrEmpty(marker.stationId)) {

                            // let detailsDiv = this.createOrGetDetailsDiv();
                            // detailsDiv.innerHTML = this.addLoadingImage();

                            this.api.getChargeDetail(marker.stationId).then(station => {
                                this.chargeDetail = this.createStationDetails(station);
                            }, err => {
                                this.globalErrorHandler(this.translate.i18n_FailedGettingStation + marker.stationId);
                            });

                        }

                    });
                } else {
                    this.googleMaps.event.addListener(marker, 'click', () => {


                        if (marker.viewPort != null &&
                            !this.isNullOrZero(marker.viewPort.upperRightLat) && !this.isNullOrZero(marker.viewPort.upperRightLon) &&
                            !this.isNullOrZero(marker.viewPort.lowerLeftLat) && !this.isNullOrZero(marker.viewPort.lowerLeftLon)) {
                            // Group View Port
                            let position = new this.googleMaps.LatLngBounds(
                                new this.googleMaps.LatLng(marker.viewPort.lowerLeftLat, marker.viewPort.lowerLeftLon),
                                new this.googleMaps.LatLng(marker.viewPort.upperRightLat, marker.viewPort.upperRightLon)
                            );

                            this.map.panToBounds(position);
                        } else {
                            // Marker position
                            this.map.panToBounds(marker.getPosition());
                        }
                        this.map.setCenter(marker.getPosition());
                        // map.panTo(event.latLng)
                        this.map.setZoom(this.map.getZoom() + 1);
                        this.closeStationDetailsDiv();

                    });
                }

                this.markers.push(marker);
            }
        }
    }

    createMarker(grouped, chargePoints, avaiablePercentage, status) {
        let canvas, ctx;
        let scale = grouped ? 1 : 0.8;
        let width = 50;
        let height = 70;

        // Find the correct status
        let mainColor = this.colors.green;
        if (status === this.stationStatus.unknown) {
            mainColor = this.colors.grau;
        } else if (status === this.stationStatus.unavailable) {
            mainColor = this.colors.red;
        }

        canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        ctx = canvas.getContext("2d");
        ctx.scale(scale, scale);

        ctx.beginPath();
        ctx.fillStyle = mainColor;
        ctx.arc((width / 2), (height / 2), (width / 2), 0, 2 * Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.strokeStyle = this.colors.white;
        ctx.lineWidth = 4;
        // make an offset corresponding to the percentage
        let offset = 1.5 * Math.PI;
        ctx.arc((width / 2), (height / 2), (width / 2) - 5, offset, (avaiablePercentage * 2 * Math.PI) + offset);
        ctx.stroke();

        ctx.beginPath();
        ctx.fillStyle = this.colors.white;

        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';

        // calculate the number of the charging points
        let chargePointsCount: any = parseInt(chargePoints, 10);
        if (chargePointsCount > 999) {
            chargePointsCount = '+' + 999;
        } else {
            chargePointsCount = chargePointsCount + '';
        }

        ctx.font = "24px Arial";
        if (chargePointsCount.length === 3) {
            ctx.font = "19px Arial";
        } else if (chargePointsCount.length === 4) {
            ctx.font = "15px Arial";
        }
        ctx.fillText(chargePointsCount, (width / 2), (height / 2));

        ctx.beginPath();
        ctx.fillStyle = mainColor;
        ctx.moveTo((width / 2) - 10, (height / 2) + (width / 2) - 1.8);
        ctx.lineTo((width / 2), (height / 2) + (width / 2) + 10 - 1.8);
        ctx.lineTo((width / 2) + 10, (height / 2) + (width / 2) - 1.8);
        ctx.fill();

        return canvas.toDataURL();
    }

    removeAllMarkers() {
        if (!this.isEmptyArray(this.markers)) {
            for (let i = 0; i < this.markers.length; i++) {
                this.markers[i].setMap(null);
            }
            this.markers = [];
        }
    }

    getExtendedBounds(initBounds) {
        let height = initBounds.getNorthEast().lat() - initBounds.getSouthWest().lat();
        let width = initBounds.getNorthEast().lng() - initBounds.getSouthWest().lng();

        let newSouthWestLat = initBounds.getSouthWest().lat() - height * this.mapBoxOffset;
        let newSouthWestLng = initBounds.getSouthWest().lng() - width * this.mapBoxOffset;

        let newNorthEastLat = initBounds.getNorthEast().lat() + height * this.mapBoxOffset;
        let newNorthEastLng = initBounds.getNorthEast().lng() + width * this.mapBoxOffset;

        return new this.googleMaps.LatLngBounds(
            new this.googleMaps.LatLng(newSouthWestLat, newSouthWestLng),
            new this.googleMaps.LatLng(newNorthEastLat, newNorthEastLng)
        );
    }

    checkMapCoordsStatus(newFromLat, newFromLon, newToLat, newToLon) {
        if (this.isNullOrZero(newFromLat) || this.isNullOrZero(newFromLon) || this.isNullOrZero(newToLat) || this.isNullOrZero(newToLon)) {
            return this.coordinatesStatus.new;
        }

        if (this.isNullOrZero(this.fromLat) || this.isNullOrZero(this.fromLon) || this.isNullOrZero(this.toLat) || this.isNullOrZero(this.toLon)) {
            return this.coordinatesStatus.new;
        }

        if (newFromLat < this.fromLat || newFromLon < this.fromLon || newToLat > this.toLat || newToLon > this.toLon) {
            return this.coordinatesStatus.bigger;
        } else {
            return this.coordinatesStatus.smaller;
        }
    }

    getColors(customColors) {
        if (customColors != null) {
            this.colors.green = customColors.green != null ? customColors.green : this.colors.green;
            this.colors.white = customColors.white != null ? customColors.white : this.colors.white;
            this.colors.red = customColors.red != null ? customColors.red : this.colors.red;
            this.colors.grau = customColors.grau != null ? customColors.grau : this.colors.grau;
            this.colors.grau70 = customColors.grau70 != null ? customColors.grau70 : this.colors.grau70;
            this.colors.black = customColors.black != null ? customColors.black : this.colors.black;
            this.colors.silver = customColors.silver != null ? customColors.silver : this.colors.silver;
            this.colors.horizonOrange = customColors.horizonOrange != null ? customColors.horizonOrange : this.colors.horizonOrange;
            this.colors.blue = customColors.blue != null ? customColors.blue : this.colors.blue;
        }
    }

    setCurrentLocation() {
        let infoWindow = new this.googleMaps.InfoWindow;

        this.geo.getCurrentPosition().then(position => {

            // set current position when allowed
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            this.setOwnLocationMarker(pos);
            infoWindow.setPosition(pos);
            this.map.setZoom(12);
            this.map.setCenter(pos);


        }, err => {
            this.handleLocationError(false, infoWindow, this.map.getCenter());
        }).then(() => {

            this.mapEvent(false);

            this.overlay = new this.googleMaps.OverlayView();
            this.overlay.draw = () => {
            };
            this.overlay.setMap(this.map);

            // Map Zoom
            this.googleMaps.event.addListener(this.map, 'zoom_changed', () => {
                this.mapEvent(false);
            });

            // Map Dragged (Scrolled)
            this.googleMaps.event.addListener(this.map, 'dragend', () => {
                this.mapEvent(true);
            });

            // Map Load
            this.googleMaps.event.addListener(this.map, 'idle', () => {
                this.mapEvent(false);
                this.googleMaps.event.clearListeners(this.map, 'idle');
            });
            this.googleMaps.event.addListener(this.map, 'tilesloaded', () => {
                this.mapEvent(false);
                this.googleMaps.event.clearListeners(this.map, 'tilesloaded');
            });

            // disable Popups
            this.googleMaps.event.addListener(this.map, 'click', (event) => {
                if (event.placeId) {
                    event.stop();
                }
                this.closeStationDetailsDiv();
            });

            this.createStyles();

        });


    }

    handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
    }

    isNullOrEmpty(value) {
        return value == null || value === '';
    }

    isNullOrZero(value) {
        return value == null || value === 0;
    }

    isEmptyArray(array) {
        return array == null || array.length === 0;
    }

    truncate(value, amount) {
        if (this.isNullOrEmpty(value)) {
            return '';
        }

        if (amount == null) {
            amount = 20;
        }

        return value.length > amount ? value.substr(0, amount) + '...' : value;
    }

    en: any = {
        'i18n_InvalidCoordinates': 'Cannot find map coordinates.',
        'i18n_ServerFailed': 'Failed to connect the server.',
        'i18n_FailedGettingStation': 'Cannot get the station: ',
        'i18n_FailedCreatingDetailsDiv': 'Cannot open station details',
        'i18n_Available': 'available',
        'i18n_km': 'km',
        'i18n_unknown': 'unknown',
        'i18n_max': 'max. ',
        'i18n_maxPlug': 'max. ',
        'i18n_kW': 'kW',
        'i18n_Operator': 'Operator',
        'i18n_OpeningHours': 'Opening hours',
        'i18n_AlwaysOpen': 'Open 24/7',
        'i18n_Tariffs': 'Tariffs',
        'i18n_FeesRemarks': 'Fees and remarks',
        'i18n_Information': 'Information',
        'i18n_AuthenticationModes': 'Authentication modes',
        'i18n_AuthenticationModesMobilityLoadingMap': 'mobility+<br />Charging card',
        'i18n_AuthenticationModesMobilityApp': 'mobility+<br />App',
        'i18n_AuthenticationModesOthers': 'Others<br />(not EnBW)',
        'i18n_SelectChargingPoints': 'Select charging points',
        'i18n_Restriction': 'Restriction',
        'i18n_PlugStatusOccupied': 'occupied',
        'i18n_PlugStatusUknown': 'unknown',
        'i18n_PlugStatusAvailable': 'available',
        'i18n_PlugStatusOutOfService': 'out of service',
        'i18n_PlugStatusReserved': 'reserved'
    };

    de: any = {
        'i18n_InvalidCoordinates': 'Kartenkoordinaten konnten nicht gefunden werden.',
        'i18n_ServerFailed': 'Fehler beim Verbinden des Servers.',
        'i18n_FailedGettingStation': 'Fehler beim Abruf der Ladestationsdaten: ',
        'i18n_FailedCreatingDetailsDiv': 'Die Stationsdetails können nicht geöffnet werden.',
        'i18n_Available': 'verfügbar',
        'i18n_km': 'km',
        'i18n_unknown': 'unbekannt',
        'i18n_max': 'max. ',
        'i18n_maxPlug': 'max. ',
        'i18n_kW': 'kW',
        'i18n_Operator': 'Betreiber',
        'i18n_OpeningHours': 'Öffnungszeiten',
        'i18n_AlwaysOpen': 'Immer geöffnet',
        'i18n_Tariffs': 'Tarife',
        'i18n_FeesRemarks': 'Gebühren und Hinweise',
        'i18n_Information': 'Informationen',
        'i18n_AuthenticationModes': 'Zugangsmöglichkeiten',
        'i18n_AuthenticationModesMobilityLoadingMap': 'mobility+<br />Ladekarte',
        'i18n_AuthenticationModesMobilityApp': 'mobility+<br />App',
        'i18n_AuthenticationModesOthers': 'Sonstige<br />(nicht EnBW)',
        'i18n_SelectChargingPoints': 'Ladepunkte anzeigen',
        'i18n_Restriction': 'Einschränkung',
        'i18n_PlugStatusOccupied': 'belegt',
        'i18n_PlugStatusUknown': 'unbekannt',
        'i18n_PlugStatusAvailable': 'verfügbar',
        'i18n_PlugStatusOutOfService': 'in Wartung',
        'i18n_PlugStatusReserved': 'reserviert'
    };

    createStyles() {
        // Add styles
        let css = document.createElement('style');
        css.type = 'text/css';
        css.innerHTML = '';

        // Colours
        css.innerHTML += '.horizonOrange {color: ' + this.colors.horizonOrange + '} ';
        css.innerHTML += '.green {color: ' + this.colors.green + '} ';
        css.innerHTML += '.black {color: ' + this.colors.black + '} ';
        css.innerHTML += '.grau70 {color: ' + this.colors.grau70 + '} ';
        css.innerHTML += '.silver {color: ' + this.colors.silver + '} ';
        css.innerHTML += '.red {color: ' + this.colors.red + '} ';

        // Fonts, Alignment
        css.innerHTML += '.small {font-size: 13px;} ';
        css.innerHTML += '.big {font-size: 15px;} ';
        css.innerHTML += '.valign-top {vertical-align: top;} ';
        css.innerHTML += '.right {float: right} ';
        css.innerHTML += '.center {text-align: center;} ';

        // Padding, Margin, Borders, Display
        css.innerHTML += '.inline-block {display: inline-block} ';
        css.innerHTML += '.width-25 {width: 25%;} ';
        css.innerHTML += '.width-75 {width: 75%;} ';
        css.innerHTML += '.width-20 {width: 20%;} ';
        css.innerHTML += '.margin-10 {margin: auto 10px;} ';
        css.innerHTML += '.border {border: 1px solid ' + this.colors.grau70 + ';} ';
        css.innerHTML += '.border {border: 1px solid rgba(240,240,240,1);} ';

        // Scrollbar design, Only for Chrome
        css.innerHTML += '::-webkit-scrollbar { width: 8px; }';
        css.innerHTML += '::-webkit-scrollbar-track { -webkit-box-shadow: 0; }';
        css.innerHTML += '::-webkit-scrollbar-thumb { background-color: #e1e1e1; outline: 0; }';

        // Custom
        css.innerHTML += '#station-details {overflow-y: auto;position: absolute;margin: 5px; z-index: 5;width: 50%; max-width: 500px; background-color: ' + this.colors.white + ';height: 98%; border-radius: 5px;padding-left: 10px;padding-right: 10px;} ';
        css.innerHTML += '.no-children-padding> * {margin: 5px 0} ';
        css.innerHTML += '.close-img {float: right; margin: 5px; cursor: pointer} ';
        css.innerHTML += '.back-button {float: left; margin: 5px; cursor: pointer} ';
        css.innerHTML += '.hr {border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);} ';
        css.innerHTML += '.icon {width: 15px; height: 15px} ';
        css.innerHTML += '.icon-table {width: 100%; padding: 0; margin: 0;} .icon-table td:first-child {width: 25%} .icon-table td:last-child {width:75%} ';
        css.innerHTML += '.dot{height: 5px;width: 5px;background-color:' + this.colors.grau70 + ';border-radius: 50%;display: inline-block;} ';
        css.innerHTML += '.loading-image {width: 70px; height: 50px;}';
        css.innerHTML += '.select-button {width: auto; font-size: 16px; font-weight: 700; background: ' + this.colors.blue + '; color: ' + this.colors.white + '; text-decoration: none;outline: none; border: #e5e5f4 4px solid; border-radius: 100px; padding: 11px 25px 9px;}';
        css.innerHTML += '.select-button:hover {cursor: pointer; background-color: #0e0ec4;}';

        // Media Queries
        css.innerHTML += '@media screen and (max-width: 800px) {#station-details {width: 60%; max-width: 100%;}} ';
        css.innerHTML += '@media screen and (max-width: 580px) {#station-details {width: 90%; max-width: 100%;}} ';
        css.innerHTML += '@media screen and (max-width: 359px) {#station-details {width: 90%; max-width: 100%;}} ';

        document.getElementsByTagName('head')[0].appendChild(css);
    }

    globalErrorHandler(error) {
        console.log('Error: ' + error);
    }

    addLoadingImage() {
        return '<img src="' + this.imagePath + 'loading.gif" class="loading-image" />';
    }

    createStationDetails(station): i_chargeDetail {

        const detail: any = {
            head: station.stationSummary,
            address: station.shortAddress,
            chargePoints: this.getChargePoints(station),
            maxPower: station.maxPowerInKw,
            types: station.plugTypeNames,
            auth: this.addAuthenticationModes(station),
            operator: station.operator,
            openingHours: station.alwaysOpen ? this.translate.i18n_AlwaysOpen : station.openingHours != null ? station.openingHours : this.translate.i18n_unknown,
            tariffs: this.addTariffs(station),
            tariffInformation: this.addTariffsInformation(station),
            additionalInfo: this.addAdditionalInfo(station),
            resctictionInfos: this.addRestrictions(station),
            original_station: station
        };

        return detail;
    }

    getChargePoints(station) {
        return {
            available: station.availableChargePoints,
            max: station.numberOfChargePoints
        }
    }

    getPlugImage(group, status, cableAttached) {
        let src = '' + 'https://ev-web.azurewebsites.net/assets/icn_plugs_';
        if (status === this.plugStatus.available) {
            src += group + (cableAttached ? '_tankrussel' : '');
        } else if (status === this.plugStatus.reserved) {
            src += group + (cableAttached ? '_grau_tankrussel' : '_grau');
        } else if (status === this.plugStatus.occupied) {
            src += group + (cableAttached ? '_grau_tankrussel' : '_grau');
        } else if (status === this.plugStatus.outOfService) {
            src += group + (cableAttached ? '_grau_beides' : '_grau_schraubschlussel');
        } else if (status === this.plugStatus.unknown) {
            src += group + (cableAttached ? '_grau_tankrussel' : '_grau');
        }

        return src + '.svg';
    }

    addAuthenticationModes(station) {

        let auth: any[] = [];

        if (this.showEnBWInfo) {
            if (station.authenticationMethods.includes('MOBILITY_PLUS_CHARGE_CARD')) {

                let item = {
                    mode: this.translate.i18n_AuthenticationModes,
                    img: 'https://ev-web.azurewebsites.net/assets/authentication_mode_card_on.svg',
                    title: this.translate.i18n_AuthenticationModesMobilityLoadingMap
                };
                auth.push(item);
            }

            if (station.authenticationMethods.includes('REMOTE')) {

                let item = {
                    mode: '',
                    img: 'https://ev-web.azurewebsites.net/assets/authentication_mode_remote_on.svg',
                    title: this.translate.i18n_AuthenticationModesMobilityApp
                };
                auth.push(item);
            }

            if (station.authenticationMethods.includes('OTHERS')) {

                let item = {
                    mode: '',
                    img: 'https://ev-web.azurewebsites.net/assets/authentication_mode_others_on.svg',
                    title: this.translate.i18n_AuthenticationModesOthers
                };
                auth.push(item);
            }
        }
        return auth;
    }

    addTariffs(station) {
        let connectors = [];
        let result = [];
        if (!this.isEmptyArray(station.chargePoints) && this.showEnBWInfo) {
            for (let i = 0; i < station.chargePoints.length; i++) {
                if (!this.isEmptyArray(station.chargePoints[i].connectors)) {
                    let point = station.chargePoints[i];
                    let status = point.status != null ? point.status : this.plugStatus.unknown;
                    for (let j = 0; j < point.connectors.length; j++) {
                        let connector = point.connectors[j];
                        let plugGroup = connector.chargePlugTypeGroup != null ? connector.chargePlugTypeGroup.toLowerCase() : '';
                        let cableAttached = connector.cableAttached != null ? connector.cableAttached : false;
                        if (connector != null) {
                            if (!this.isConnectorExist(connectors, connector)) {
                                connectors.push(connector);
                                let name = connector.plugTypeName != null ? connector.plugTypeName : '';
                                let tariffDescription = connector.tariffInfo != null && connector.tariffInfo.tariffDescription != null ? connector.tariffInfo.tariffDescription : '';

                                let con = {
                                    name: name,
                                    img: this.getPlugImage(plugGroup, status, cableAttached),
                                    tariffDescription: tariffDescription
                                };
                                result.push(con);
                            }
                        }
                    }
                }
            }

        }
        return result;
    }

    isConnectorExist(connectors, connector) {
        if (this.isEmptyArray(connectors) || connector == null || connector.chargePlugTypeGroup == null) {
            return false;
        }

        for (let i = 0; i < connectors.length; i++) {
            if (connectors[i] != null && connectors[i].chargePlugTypeGroup != null) {
                if (connectors[i].chargePlugTypeGroup === connector.chargePlugTypeGroup) {
                    return true;
                }
            }
        }

        return false;
    }

    addTariffsInformation(station) {

        let information: any;

        if (this.showEnBWInfo) {
            if (station.tariffInformations instanceof Object) {
                if (!this.isNullOrEmpty(station.tariffInformations)) {
                    information = {
                        head: this.translate.i18n_FeesRemarks,
                        img: 'https://ev-web.azurewebsites.net/assets/icn_hinweise_info.svg',
                        content: [
                            station.tariffInformations
                        ]
                    }
                }
            } else {
                if (!this.isEmptyArray(station.tariffInformations)) {
                    let content: any[] = [];
                    for (let i = 0; i < station.tariffInformations.length; i++) {
                        content.push(station.tariffInformations[i]);
                    }
                    information = {
                        head: this.translate.i18n_FeesRemarks,
                        img: 'https://ev-web.azurewebsites.net/assets/icn_hinweise_info.svg',
                        content: content
                    }
                }
            }
        }
        return information;
    }

    addAdditionalInfo(station) {

        let content = '';
        let split: any;
        if (!this.isNullOrEmpty(station.additionalInfo)) {
            split = station.additionalInfo != null ? station.additionalInfo.split('\r\n') : '';
            if (!this.isEmptyArray(split)) {
                split = split.join('<br />');
            }
        }

        let info: any = {
            head: this.translate.i18n_Information,
            content: split
        };

        return info;
    }

    addRestrictions(station) {
        let content: any;

        if (!this.isNullOrEmpty(station.restrictions)) {
            content = {
                head: this.translate.i18n_Restriction,
                restrictions: station.restrictions
            }
        }
        return content;
    }

    closeDetail() {
        this.chargeDetail = null;
        if(this.map.zoom > 13)
            this.map.setZoom(13);
    }

    loadChargeStations(chargeDetail: i_chargeDetail) {
        this.navCtrl.push(ChargeStationsPage, chargeDetail);
    }

    getDistance(p1, p2) {
        let R = 6378137;
        let dLat = this.rad(p2.lat - p1.lat());
        let dLong = this.rad(p2.lng - p1.lng());
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat())) * Math.cos(this.rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;
    };

    rad(x) {
        return x * Math.PI / 180;
    }


    initDrag(){
        const balltrigger = document.querySelector('#container-drag-element-trigger');
        const ball = document.querySelector('#container-drag-element');
        const map = styler(document.querySelector('#map-wrapper'));
        const divStyler = styler(ball);
        const ballY = value(0, (e) => {
                let listHeight = 100-((e.y/document.body.clientHeight)*100);
                let mapHeight = ((e.y/document.body.clientHeight)*100);
                divStyler.set({height:listHeight+'%'});
                map.set({height:mapHeight+'%'});
            }
        );

        listen(balltrigger, 'mousedown touchstart')
            .start((e) => {
                e.preventDefault();
                pointer().start(ballY);
            });

        listen(document, 'mouseup touchend')
            .start((e) => {
                ballY.stop();
                let currentElementHeight= this.getNumberHeight(divStyler.get('height'));

                if(currentElementHeight<40){
                    divStyler.set({height:15+'%'});
                    map.set({height:85+'%'});
                }
            });
    }

    listMoveBottom(){
        const ball = styler(document.querySelector('#container-drag-element'));
        const map = styler(document.querySelector('#map-wrapper'));

        if(this.getNumberHeight(ball.get('height'))>40){
            tween({
                from: { height: '55%' },
                to: { height: 15 + '%' },
                ease: easing.backOut,
                duration: 1500
            }).start(ball.set);

            tween({
                from: { height: '45%' },
                to: { height: 85 + '%' },
                ease: easing.backOut,
                duration: 1500
            }).start(map.set);
        }
    }

    listMoveTop(){
        const ball = styler(document.querySelector('#container-drag-element'));
        const map = styler(document.querySelector('#map-wrapper'));

        if(this.getNumberHeight(ball.get('height'))<40){
            tween({
                from: { height: '15%' },
                to: { height: 55 + '%' },
                ease: easing.backOut,
                duration: 1500
            }).start(ball.set);

            tween({
                from: { height: '85%' },
                to: { height: 45 + '%' },
                ease: easing.backOut,
                duration: 1500
            }).start(map.set);
        }
    }

    getNumberHeight(height:any):number{
        return height.substring(0, height.length - 1)*1;
    }


}
