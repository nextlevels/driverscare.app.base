import {Component} from '@angular/core';
import {DateTime, IonicPage, NavController, NavParams, Toggle} from 'ionic-angular';
import {DatePicker} from "@ionic-native/date-picker";
import {ApiProvider} from "../../providers/api/api";
import {GlobalProvider} from "../../providers/global/global";
import * as moment from "moment";
import {Dealer} from "../../models/Dealer";
import {StorageProvider} from "../../providers/storage/storage";
import {_TEST_MODE, _TEST_MODE_SETTINGS, _TEST_USER} from "../../config";


@Component({
    selector: 'page-bookinghelper',
    templateUrl: 'bookinghelper.html',
})
export class BookinghelperPage {

    dates: string[] = [];
    datesFormated: string[] = [];
    chooseSendCarInformations: boolean = true;
    myDate: string = "";
    minDate: string = "";
    maxDate: string = "";
    dealer: Dealer;

    related_data: any = {};
    confirmDealerMeeting: boolean = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, public datePicker: DatePicker, public api: ApiProvider, public global: GlobalProvider) {

    }

    ionViewDidLoad() {
        this.minDate = moment().format("YYYY-MM-DD");
        this.maxDate = moment().add("years", 1).format("YYYY-MM-DD");

        this.dealer = this.navParams.get('param').param;
        this.related_data = this.navParams.get('param').related_data;

        if (this.related_data) {
            this.dates.push(this.related_data.time);
            this.confirmDealerMeeting = true;
        }

    }

    changeSendCarValue(sendCar: Toggle) {
        this.chooseSendCarInformations = sendCar._value
    }

    bookForm() {

        if(this.confirmDealerMeeting){
            let id = this.related_data.id;
            this.api.confirmBook(id).then(data => {
                this.global.showAlert('Der Termin wurde bestätigt');
            },error => {
                this.global.showAlert(error, true);
            });
            return;
        }

        for (let date of this.dates) {
            this.datesFormated.push(moment(date, "DD.MM.YYYY HH:mm").format('YYYY-MM-DD HH:mm:ss'));
        }

        let dealerEmail : string = (_TEST_MODE && _TEST_MODE_SETTINGS.dealer_test) ? _TEST_USER.dealer_email : (this.related_data ? this.related_data.dealer_email : this.dealer.email);
        if(!dealerEmail && this.dealer.website){
            dealerEmail = "info@" + this.dealer.website.replace(/^(https?:\/\/)([a-z]{3}[0-9]?\.)?(\w+)(\.[a-zA-Z]{2,3})(\.[a-zA-Z]{2,3})?.*$/, '$3$4$5');
        }


        let request = {
            send_vehicle_information: this.chooseSendCarInformations,
            dealer_name: this.related_data ? this.related_data.dealer_name : this.dealer.name,
            dealer_email: dealerEmail,
            process_id: this.related_data ? this.related_data.process_id : StorageProvider.activeProcess.id,
            service_id: StorageProvider.activeService,
            appointments: this.datesFormated
        };

        this.api.book(request)
            .then(data => {
                this.global.showPromptWithCallback("Termine wurden angefragt").then(data => {
                    if(data){
                        this.navCtrl.pop();
                    }
                });
            }, error => {
                this.global.showAlert(error.errorMessage, true);
            });
    }

    addDate(date: DateTime) {
        if (this.confirmDealerMeeting) this.confirmDealerMeeting = false;
        this.dates.push(date._text);
    }

    removeDate(i: number = 0) {
        if (this.confirmDealerMeeting) this.confirmDealerMeeting = false;
        this.dates.splice(i, 1);
    }

    // openDatePicker() {
    //     const options = {
    //         date: new Date(),
    //         mode: 'date'
    //     };
    //     this.datePicker.show(options).then(data => {
    //         console.log(data);
    //     });
    //
    // }


}
