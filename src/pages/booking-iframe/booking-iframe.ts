import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";
import {StorageProvider} from "../../providers/storage/storage";

@Component({
  selector: 'page-booking-iframe',
  templateUrl: 'booking-iframe.html',
})
export class BookingIframePage {

  bookingUrl: any = "";
  dealer: any = "";
  vehicle: any = "";

  @ViewChild('iframe') iFrame: ElementRef;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private sanitizer: DomSanitizer) {

    this.dealer = this.navParams.get('param').param;
    this.vehicle = StorageProvider.vehicle;

    /*
    Wartung 9 1ON435
    Garantie 11 1ON435
    HU/AU 24	1ON131
    Hol-/Bringservice 23 1ON435
    Ersatzreifen 7 1ON109
    Inspektion 10 1ON435
    UVV 18 1EI997
    Reifenwechsel 8 1ON109
     */

    // Express-Termin
    let _serviceNumber: string;


    switch (StorageProvider.activeService)
    {
      case '9':
      case '11':
      case '23':
      case '10':
        _serviceNumber = '1ON435';
        break;
      case '24':
        _serviceNumber = '1ON131';
        break;
      case '7':
      case '8':
        _serviceNumber = '1ON109';
        break;
      case '18':
        _serviceNumber = '1EI997';
        break;
      default:
        _serviceNumber = '1ON449';
    }

    let _customerNumber: string = '912312312';
    if(StorageProvider.activeSP!=null){
      if(StorageProvider.activeSP.customer_number !== 'undefined' && StorageProvider.activeSP.customer_number !=''){
        _customerNumber = StorageProvider.activeSP.customer_number;
      }
    }

    let bookingUrl = 'https://www.atu.de/terminvereinbarung/service/';
    bookingUrl += '?store='+this.dealer.website_booking;
    bookingUrl += '&serviceNr='+_serviceNumber;
    bookingUrl += '&salutation='+this.vehicle.driver.salutation;
    bookingUrl += '&name='+this.vehicle.driver.first_name;
    bookingUrl += '&surname='+this.vehicle.driver.last_name;
    bookingUrl += '&email='+this.vehicle.driver.email;

    if(this.vehicle.driver.private_address != null && this.vehicle.driver.private_address.mobile_number != ''){
      bookingUrl += '&phone='+this.vehicle.driver.private_address.mobile_number;
    }else if(this.vehicle.driver.business_address != null && this.vehicle.driver.business_address.mobile_number != ''){
      bookingUrl += '&phone='+this.vehicle.driver.business_address.mobile_number;
    }

    bookingUrl += '&licensePlate='+this.vehicle.license_plate;
    bookingUrl += '&businessCustomer=true';
    bookingUrl += '&company='+this.vehicle.company.name;
    bookingUrl += '&customerNumber='+_customerNumber;

    if(this.vehicle.company.address){
      bookingUrl += '&street='+this.vehicle.company.address.street;
      bookingUrl += '&streetNumber='+this.vehicle.company.address.house_number;
      bookingUrl += '&zip='+this.vehicle.company.address.postal_code;
      bookingUrl += '&city='+this.vehicle.company.address.city;
    }else{
      bookingUrl += '&street=';
      bookingUrl += '&streetNumber=';
      bookingUrl += '&zip=';
      bookingUrl += '&city=';
    }

    bookingUrl += '&source=fleet-hub';


    this.bookingUrl = this.sanitizer.bypassSecurityTrustResourceUrl(bookingUrl);
  }

  ionViewDidLoad() {
    this.bindEvent(window, 'APPOINTMENT_BOOKING_COMPLETED', this.saveEvent.bind(this));
  }

  back(){
    this.navCtrl.pop();
  }

  saveEvent(){
    this.navCtrl.pop();
  }

  bindEvent(element, eventName, eventHandler) {
    if (element.addEventListener) {
      element.addEventListener(eventName, eventHandler, false);
    } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, eventHandler);
    }
  }

}
