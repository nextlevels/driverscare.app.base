import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {ChargeStartPage} from "../charge-start/charge-start";
import {i_chargeDetail} from "../maplist-enbw/maplist-enbw";


@Component({
    selector: 'page-charge-stations',
    templateUrl: 'charge-stations.html',
})
export class ChargeStationsPage {

    charge: i_chargeDetail;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams) {
    }

    ionViewDidLoad() {
        if (this.navParams) {
            this.charge = this.navParams.data;
        }
    }

    close() {
        this.navCtrl.pop();
    }

    goToChargeStartPage(item) {
        if (item.status === 'AVAILABLE')
            this.navCtrl.push(ChargeStartPage, item)
    }


}
