
export class ApiResponse{

    success: boolean = true;
    error: boolean = false;
    errorMessage: any= null;
    errorCode: number = 0;
    count: number = 0;
    responseType: any = null;
    response: any = null;

    constructor(
        success: boolean = true,
        error: boolean = false,
        errorMessage: any= null,
        errorCode: number = 0,
        count: number = 0,
        responseType: any = null,
        response: any = null
    ){
        this.success = success;
        this.error = error;
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.count = count;
        this.responseType = responseType;
        this.response = response;
    }
}

