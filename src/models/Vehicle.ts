export class Vehicle{
    name : string = "";
    vehicle_number : string = "";
    brand : string = "";
    type : string = "";
    model : string = "";
    first_registration : string = "";
    fin : string = "";
    insurance : string = "";
    insurance_number : string = "";
    emissions_class : string = "";
    fuel_type : string = "";
    kw_hp : string = "";
    license_plate : string = "";
    partial_cover : string = "";
    full_cover : string = "";
    tyre_partner : string = "";
    pool_id : string = "";
    leasing_start : string = "";
    leasing_end : string = "";
    leasing_contract_number : string = "";
    gross_list_price : string = "";
    lessor_id : string = "";
    wheel_size_winter : string = "";
    wheel_size_summer : string = "";
    image : any = null;
    registration_document : any = null;
    insurance_card : any = null;
    driver : any = null;
    lessor: i_lessor = null;
    mileage : number = 0;
    sb_full_cover : number = 0;
    sb_partial_cover : number = 0;
    company : i_company = null;


    constructor(
    name : string = "",
    vehicle_number : string = "",
    brand : string = "",
    type : string = "",
    model : string = "",
    first_registration : string = "",
    fin : string = "",
    insurance : string = "",
    insurance_number : string = "",
    emissions_class : string = "",
    fuel_type : string = "",
    kw_hp : string = "",
    license_plate : string = "",
    partial_cover : string = "",
    full_cover : string = "",
    tyre_partner : string = "",
    pool_id : string = "",
    leasing_start : string = "",
    leasing_end : string = "",
    leasing_contract_number : string = "",
    gross_list_price : string = "",
    lessor_id : string = "",
    wheel_size_winter : string = "",
    wheel_size_summer : string = "",
    image : any = null,
    insurance_card : any = null,
    driver : any = null,
    registration_document : any = null,
    lessor : i_lessor = null,
    mileage : number = 0,
    sb_full_cover : number = 0,
    sb_partial_cover : number = 0,
    company : i_company = null
    ){
        this.name = name;
        this.vehicle_number = vehicle_number;
        this.brand = brand;
        this.type = type;
        this.first_registration = first_registration;
        this.fin = fin;
        this.insurance = insurance;
        this.insurance_number = insurance_number;
        this.emissions_class = emissions_class;
        this.fuel_type = fuel_type;
        this.license_plate = license_plate;
        this.partial_cover = partial_cover;
        this.full_cover = full_cover;
        this.tyre_partner = tyre_partner;
        this.pool_id = pool_id;
        this.leasing_end = leasing_end;
        this.leasing_contract_number = leasing_contract_number;
        this.gross_list_price = gross_list_price;
        this.lessor_id = lessor_id;
        this.leasing_start = leasing_start;
        this.wheel_size_winter = wheel_size_winter;
        this.wheel_size_summer = wheel_size_summer;
        this.kw_hp = kw_hp;
        this.image = image;
        this.registration_document = registration_document;
        this.insurance_card = insurance_card;
        this.driver = driver;
        this.lessor = lessor;
        this.mileage = mileage;
        this.sb_full_cover = sb_full_cover;
        this.sb_partial_cover = sb_partial_cover;
        this.company = company;
    }
}

interface i_lessor{
    name: string;
}

interface i_company{
    address_id : number;
    app_color : string;
    app_logo : string;
    bank_name : string;
    bic : string;
    created_at : string;
    deleted_at : string;
    iban : string;
    id : string;
    policy : string;
    invoice_address_id : number;
    name : string;
    reseller_id : number;
    service_provider_editable : string;
    updated_at : string;
    user_id : number;
    reseller: any;
    drivers: i_driver[];
}

interface i_driver{
    business_address_id : number;
    company_id : number;
    created_at : string;
    drivers_license_number : string;
    email : string;
    first_name : string;
    fleet_id : number;
    id : string;
    is_active : boolean;
    last_name : string;
    private_address_id : number;
    push_token : string;
    salutation : string;
    title : string;
    updated_at : string;
    user_id : number;
    vehicle_id : number;
    private_address : any;
    business_address : any;
}
