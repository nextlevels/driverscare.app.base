export class User_Token{

    token : string = "";
    email : string = "";
    password : string = "";
    user : User = null;
    logout: boolean = false;

    constructor(
        token : string = "",
        user : User = null,
        logout : boolean = false
    ){
        this.token = token;
        this.user = user;
        this.logout = logout;
    }
}

export class User{

    id : number = 0;
    name : string = "";
    email : string = "";
    permissions : string = "";
    is_activated : boolean = false;
    activated_at : string = "";
    last_login : string = "";
    created_at : string = "";
    updated_at : string = "";
    username : string = "";
    surname : string = "";
    deleted_at : string = "";
    last_seen : string = "";
    is_guest : number = 0;
    is_superuser : number = 0;

    constructor(
        id: number = 0,
        name: string = "",
        email: string = "",
        permissions: string = "",
        is_activated: boolean = false,
        activated_at: string = "",
        last_login: string = "",
        created_at: string = "",
        updated_at: string = "",
        username: string = "",
        surname: string = "",
        deleted_at: string = "",
        last_seen: string = "",
        is_guest: number = 0,
        is_superuser: number = 0
    ){
        this.id = id;
        this.name = name;
        this.email = email;
        this.permissions = permissions;
        this.is_activated = is_activated;
        this.activated_at = activated_at;
        this.last_login = last_login;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.username = username;
        this.surname = surname;
        this.deleted_at = deleted_at;
        this.last_seen = last_seen;
        this.is_guest = is_guest;
        this.is_superuser = is_superuser;
    }
}

