export class Card {

    id: number = 0;
    name: string = '';
    card_color: string = '';
    code_image: any = null;
    service_provider: any = null;
    code_type: string = '';
    code_number : string = '';
    is_virtual: boolean = false;

    constructor(
        name: string = '',
        card_color: string = '',
        code_image: any = null,
        service_provider: any = null,
        code_type: string = '',
        code_number: string = '',
        is_virtual: boolean = false,
    ) {
        this.name = name;
        this.card_color = card_color;
        this.code_image = code_image;
        this.code_type = code_type;
        this.code_number = code_number;
        this.service_provider = service_provider;
        this.is_virtual = is_virtual;
    }
}
