import {Process} from "../models/Process";

export class ProcessCategory{
    id : number = 0;
    name : string = "";
    category_icon : any = null;
    processes : Process[] = [];
    hidden: boolean = false;

    constructor()

    constructor(
        id : number = 0,
        name : string = "",
        category_icon : any = null,
        processes : Process[] = [],
        hidden: boolean = false

){
        this.id = id;
        this.name = name;
        this.category_icon = category_icon;
        this.processes = processes;
        this.hidden = hidden
    }
}