export class ProcessInput{
    id : number = 0;
    field_name : string = "";
    field_value : string = "";
    initial : boolean = false;


    constructor(
        id : number = 0,
        field_name : string = "",
        field_value : string = "",
        initial : boolean = false

){
        this.id = id;
        this.field_name = field_name;
        this.field_value = field_value;
        this.initial = initial;
    }
}