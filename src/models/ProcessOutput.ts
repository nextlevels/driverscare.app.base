import {ProcessInput} from "./ProcessInput";

export class ProcessOutput{
    id : number = 0;
    field_name : string = "";
    field_value : string = "";
    process_input : ProcessInput;


    constructor(
        id : number = 0,
        field_name : string = "",
        field_value : string = "",
        process_input : ProcessInput = null

){
        this.id = id;
        this.field_name = field_name;
        this.field_value = field_value;
        this.process_input = process_input;
    }
}