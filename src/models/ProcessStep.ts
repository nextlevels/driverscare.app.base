import {ProcessInput} from "./ProcessInput";
import {ProcessOutput} from "./ProcessOutput";

export class ProcessStep{
    id : number = 0;
    name : string = "";
    description : string = "";
    initial : boolean = false;
    process_inputs : ProcessInput[];
    process_outputs : ProcessOutput[];


    constructor(
        id : number = 0,
        name : string = "",
        description : string = "",
        initial :  boolean = false,
        process_inputs : ProcessInput[] = [],
        process_outputs : ProcessOutput[] = [],

){
        this.id = id;
        this.name = name;
        this.initial = initial;
        this.description = description;
        this.process_inputs = process_inputs;
        this.process_outputs = process_outputs;
    }
}