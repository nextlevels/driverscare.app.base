export interface ServiceProvider{
    address_id:string,
    api_auth_method:string,
    api_headers:string,
    api_method:string,
    api_password:string,
    api_url:string,
    api_username:string,
    booking_url:string,
    card_code_type:string,
    card_color:string,
    created_at:string,
    cron_frequency:string,
    csv_path:string,
    email:string,
    google_maps_active:string,
    google_maps_key:string,
    google_maps_radius:string,
    id:number,
    is_api:number,
    is_cron:number,
    last_cron:string,
    name:string,
    updated_at:string,
    file_banner: i_file_banner,
    file_banner_image: i_file_banner,
    is_dealer_instance : boolean,
    dealer_instance_id : number,
    dealer_instance : ServiceProvider
    action_label: string;
    action_type: string;
    action_url: string;
    customer_number: string;
}

export interface i_file_banner{
    content_type: string,
    created_at: string,
    description: string,
    disk_name: string,
    extension: string,
    field: string,
    file_name: string,
    file_size: number,
    id: number,
    path: string,
    sort_order: number,
    title: string,
    updated_at: string
}
