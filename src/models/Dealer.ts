import {ServiceProvider} from "./ServiceProvider";

export class Dealer {
    id: number = 0;
    service_provider: ServiceProvider = null;
    service_provider_id: string = '';
    name: string = '';
    street: string = '';
    house_number: string = '';
    city: string = '';
    postal_code: string = '';
    country: string = '';
    extra_information: string = '';
    telephone: string = '';
    website: string = '';
    website_booking: string = '';
    places_id: string = '';
    lat: number = 0;
    long: number = 0;
    file_banner: any = null;
    email: string = '';
    distance : any = 0;

    constructor(
        id: number = 0,
        service_provider_id: string = '',
        name: string = '',
        street: string = '',
        house_number: string = '',
        city: string = '',
        postal_code: string = '',
        country: string = '',
        extra_information: string = '',
        places_id: string = '',
        telephone: string = '',
        lat: number = 0,
        long: number = 0,
        website: string = '',
        file_banner: string = null,
        website_booking: string = null,
        email: string = '',
        service_provider: ServiceProvider = null,
        distance : any = null,

    ) {
        this.id = id;
        this.service_provider_id = service_provider_id;
        this.name = name;
        this.website = website;
        this.website_booking = website_booking;
        this.street = street;
        this.house_number = house_number;
        this.city = city;
        this.postal_code = postal_code;
        this.country = country;
        this.extra_information = extra_information;
        this.telephone = telephone;
        this.lat = lat;
        this.long = long;
        this.file_banner = file_banner;
        this.places_id = places_id;
        this.email = email;
        this.service_provider = service_provider;
        this.distance = distance;

    }
}