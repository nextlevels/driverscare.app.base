import {ProcessStep} from "./ProcessStep";

export class Process {
    id: number = 0;
    name: string = "";
    display_name: string = "";
    description: string = "";
    policy: string = "";
    companies: company[] = [];
    process_steps: ProcessStep[];
    fields:any;


    constructor(
        id: number = 0,
        name: string = "",
        display_name: string,
        policy: string = "",
        description: string = "",
        process_steps: ProcessStep[] = [],
        companies: company[] = [],
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.process_steps = process_steps;
        this.policy = policy;
        this.companies = companies;
        this.display_name = display_name
    }
}

interface company{
    address_id: number;
    app_color: string;
    app_logo: string;
    bank_name: string;
    bic: string;
    created_at: string;
    deleted_at: string;
    drivers: string;
    iban: string;
    id: number;
    invoice_address_id: string;
    name: string;
    pivot: company_pivot;
    reseller_id: string;
    service_provider_editable: string;
    updated_at: string;
    user_id: string;
    reseller: any;
}

interface company_pivot{
    c_id: number;
    custom_name: string;
    p_id: string;
    policy: string;
    sort: number;
    fields: any;
}
