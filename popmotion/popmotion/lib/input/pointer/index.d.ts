import { Action } from '../../action';
import { PointerProps } from './types';
declare const _default: ({ x, y, ...props }?: PointerProps) => Action;
export default _default;
