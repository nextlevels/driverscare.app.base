import { Action } from '../../action';
import { PointerProps } from './types';
declare const mouse: ({ preventDefault }?: PointerProps) => Action;
export default mouse;
