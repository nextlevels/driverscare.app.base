import { Action } from '../action';
declare const crossfade: (a: Action, b: Action) => Action;
export default crossfade;
