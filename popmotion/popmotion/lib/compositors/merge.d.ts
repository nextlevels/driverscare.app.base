import { Action } from '../action';
declare const merge: (...actions: Action[]) => Action;
export default merge;
