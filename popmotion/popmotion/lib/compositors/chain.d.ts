import { Action } from '../action';
declare const chain: (...actions: Action[]) => Action;
export default chain;
