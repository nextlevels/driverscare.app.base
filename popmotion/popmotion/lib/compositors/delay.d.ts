import { Action } from '../action';
declare const delay: (timeToDelay: number) => Action;
export default delay;
