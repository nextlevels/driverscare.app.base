import { Action } from '../action';
declare const schedule: (scheduler: Action, schedulee: Action) => Action;
export default schedule;
