import { Action } from '../action';
declare const _default: (...actions: Action[]) => Action;
export default _default;
