export declare type Predicate = (v?: any) => boolean;
export declare type Props = {
    [key: string]: any;
};
