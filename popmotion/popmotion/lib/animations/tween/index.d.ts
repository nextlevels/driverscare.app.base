import { Action } from '../../action';
import { TweenProps } from './types';
declare const tween: (props?: TweenProps) => Action;
export default tween;
