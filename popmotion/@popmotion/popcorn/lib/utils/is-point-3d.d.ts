import { Point, Point3D } from '../types';
declare const _default: (point: Point) => point is Point3D;
export default _default;
