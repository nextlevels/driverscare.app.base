export declare type RangeFunction = (min: number, max: number, v: number) => any;
declare const _default: (func: RangeFunction) => (min: number, max: number, v?: number) => any;
export default _default;
