import { RGBA, HSLA } from 'style-value-types';
export declare const mixArray: (from: (string | number | RGBA | HSLA)[], to: (string | number | RGBA | HSLA)[]) => (v: number) => (string | number | RGBA | HSLA)[];
export declare const mixComplex: (from: string, to: string) => Function;
