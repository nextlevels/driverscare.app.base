declare const _default: (num: number, precision?: number) => number;
export default _default;
