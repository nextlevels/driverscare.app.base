export declare const springForce: (alterDisplacement?: Function) => (min: number, max: number, v?: number) => any;
export declare const springForceLinear: (min: number, max: number, v?: number) => any;
export declare const springForceExpo: (min: number, max: number, v?: number) => any;
