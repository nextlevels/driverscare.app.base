declare const _default: (from: number, to?: number) => (v: number) => number;
export default _default;
