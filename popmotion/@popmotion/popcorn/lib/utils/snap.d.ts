declare const _default: (points: number | number[]) => (v: number) => number;
export default _default;
