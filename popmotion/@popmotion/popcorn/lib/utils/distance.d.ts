import { Point } from '../types';
declare type _Point = Point | number;
declare const _default: (a: _Point, b?: _Point) => number;
export default _default;
