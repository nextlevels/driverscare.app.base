export declare type Check = (v: any) => boolean;
export declare type Apply = (v: any) => any;
declare const _default: (check: Check, apply: Apply) => (v: any) => any;
export default _default;
