import { Point } from '../types';
declare const _default: (a: Point, b?: Point) => number;
export default _default;
