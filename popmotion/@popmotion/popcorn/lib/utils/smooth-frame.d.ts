declare const _default: (prevValue: number, nextValue: number, duration: number, smoothing?: number) => number;
export default _default;
