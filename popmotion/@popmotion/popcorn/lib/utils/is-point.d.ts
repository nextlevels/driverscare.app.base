import { Point } from '../types';
declare const _default: (point: Object) => point is Point;
export default _default;
