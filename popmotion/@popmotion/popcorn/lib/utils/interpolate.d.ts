import { Easing } from '@popmotion/easing';
declare const _default: (input: number[], output: number[] | string[], rangeEasing?: Easing[]) => (v: number) => any;
export default _default;
