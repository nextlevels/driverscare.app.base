declare const _default: (strength?: number) => (v: number) => number;
export default _default;
