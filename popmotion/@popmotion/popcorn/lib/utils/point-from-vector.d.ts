import { Point2D } from '../types';
declare const _default: (origin: Point2D, angle: number, distance: number) => {
    x: number;
    y: number;
};
export default _default;
