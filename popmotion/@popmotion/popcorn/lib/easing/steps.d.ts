export declare type Direction = 'start' | 'end';
declare const _default: (steps: number, direction?: Direction) => (progress: number) => any;
export default _default;
