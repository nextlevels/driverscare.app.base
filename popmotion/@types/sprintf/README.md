# Installation
> `npm install --save @types/sprintf`

# Summary
This package contains type definitions for sprintf (https://github.com/maritz/node-sprintf).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sprintf

Additional Details
 * Last updated: Tue, 29 Aug 2017 21:33:33 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Carlos Ballesteros Velasco <https://github.com/soywiz>, BendingBender <https://github.com/BendingBender>.
