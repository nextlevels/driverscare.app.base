# Installation
> `npm install --save @types/crypto-js`

# Summary
This package contains type definitions for crypto-js (https://github.com/evanvosberg/crypto-js).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/crypto-js

Additional Details
 * Last updated: Wed, 25 Jul 2018 22:05:07 GMT
 * Dependencies: none
 * Global values: CryptoJS

# Credits
These definitions were written by Michael Zabka <https://github.com/misak113>.
