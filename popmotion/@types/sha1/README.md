# Installation
> `npm install --save @types/sha1`

# Summary
This package contains type definitions for sha1 (https://github.com/pvorb/node-sha1).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sha1

Additional Details
 * Last updated: Wed, 25 Oct 2017 16:18:58 GMT
 * Dependencies: node
 * Global values: none

# Credits
These definitions were written by Bill Sourour <https://github.com/arcdev1>.
